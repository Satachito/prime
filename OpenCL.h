#pragma	once

inline	void
CL( cl_int p, const char* pFile, int pLine )
{	if ( p )
	{	fprintf( stderr, "%s:%d ERROR:%d\n", pFile, pLine, p );
		throw p;
	}
}

#define	CL( p )	CL( p, __FILE__, __LINE__ )

inline	void
Dump()
{
	cl_uint wNumPlatforms = 10;
	cl_platform_id wPlatforms[ wNumPlatforms ];
	CL( clGetPlatformIDs( wNumPlatforms, wPlatforms, &wNumPlatforms ) );
    printf( "NumPlatforms：%d\n", wNumPlatforms );
	for ( int j = 0; j < wNumPlatforms; j++ )
	{	size_t wSize;
		char wName[ 1024 ];
	
		wSize = sizeof( wName );
		CL( clGetPlatformInfo( wPlatforms[ j ], CL_PLATFORM_NAME, wSize, wName, &wSize ) );
		printf( "	Platform:%s\n", wName );

		cl_uint wNumDevices;
		CL( clGetDeviceIDs( wPlatforms[ j ], CL_DEVICE_TYPE_ALL, 0, NULL, &wNumDevices ) );
		cl_device_id wDeviceIDs[ wNumDevices ];
		CL( clGetDeviceIDs( wPlatforms[ j ], CL_DEVICE_TYPE_ALL, wNumDevices, wDeviceIDs, &wNumDevices ) );
		printf( "	NumDevices:%d\n", wNumDevices );
		
		for ( int i = 0; i < wNumDevices; i++ )
		{
			wSize = sizeof( wName );
			CL( clGetDeviceInfo( wDeviceIDs[ i ], CL_DEVICE_NAME, wSize, wName, &wSize ) );
			printf( "		ID:%lx	Device:%s\n", (long)wDeviceIDs[ i ], wName );
		
			cl_uint	wMaxUnits;
			wSize = sizeof( wMaxUnits );
			CL( clGetDeviceInfo( wDeviceIDs[ i ], CL_DEVICE_MAX_COMPUTE_UNITS, wSize, &wMaxUnits, &wSize ) );
			printf("		Max units:%u\n", wMaxUnits );
			
			cl_uint	wMaxDimensions;
			wSize = sizeof( wMaxDimensions );
			CL( clGetDeviceInfo( wDeviceIDs[ i ], CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS, wSize, &wMaxDimensions, &wSize ) );
			printf( "		Max dimensions:%u\n", wMaxDimensions );

			size_t	wMaxGroupSize;
			wSize = sizeof( wMaxGroupSize );
			CL( clGetDeviceInfo( wDeviceIDs[ i ], CL_DEVICE_MAX_WORK_GROUP_SIZE, wSize, &wMaxGroupSize, &wSize ) );
			printf( "		Max group size:%zu\n", wMaxGroupSize );

			size_t	wMaxItemSizes[ wMaxDimensions ];
			wSize = sizeof( wMaxItemSizes );
			CL( clGetDeviceInfo( wDeviceIDs[ i ], CL_DEVICE_MAX_WORK_ITEM_SIZES, wSize, wMaxItemSizes, &wSize ) );
			for ( int i = 0; i < wMaxDimensions; i++ ) printf( "			Work item size:%zu\n", wMaxItemSizes[ i ] );

			cl_uint	wPreferredLong;
			wSize = sizeof( wPreferredLong );
			CL( clGetDeviceInfo( wDeviceIDs[ i ], CL_DEVICE_PREFERRED_VECTOR_WIDTH_LONG, wSize, &wPreferredLong, &wSize ) );
			printf( "		Pref long:%u\n", wPreferredLong );

			size_t	wDeviceAddressBits;
			wSize = sizeof( wDeviceAddressBits );
			CL( clGetDeviceInfo( wDeviceIDs[ i ], CL_DEVICE_ADDRESS_BITS, wSize, &wDeviceAddressBits, &wSize ) );
			printf( "		DeviceAddressBits:%zu\n", wDeviceAddressBits );

			cl_ulong	wGlobalMemSize;
			wSize = sizeof( wGlobalMemSize );
			CL( clGetDeviceInfo( wDeviceIDs[ i ], CL_DEVICE_GLOBAL_MEM_SIZE, wSize, &wGlobalMemSize, &wSize ) );
			printf( "		GlobalMemSize:%lluM\n", wGlobalMemSize / 1024 / 1024 );

			printf("\n");
		}
	}
}


inline	cl_ndrange
GCLNDRange( void* pKernel, size_t p )
{	assert( p );

	size_t	w;
	size_t	wSize = sizeof( w );
	gcl_get_kernel_block_workgroup_info( pKernel, CL_KERNEL_WORK_GROUP_SIZE, wSize, &w, &wSize );
	assert( w );
	p = ( p + w - 1 ) / w * w;
	cl_ndrange v =
	{	1
	,	{ 0, 0, 0 }
	,	{ p, 0, 0 }
	,	{ w, 0, 0 }
	};
	return v;
}

inline	cl_ndrange
CLNDRange( cl_kernel pKernel, cl_device_id pDeviceID, size_t p )
{	assert( p );

	size_t	w;
	size_t	wSize = sizeof( w );
	CL( clGetKernelWorkGroupInfo( pKernel, pDeviceID, CL_KERNEL_WORK_GROUP_SIZE, wSize, &w, &wSize ) );
	assert( w );
	p = ( p + w - 1 ) / w * w;
	cl_ndrange v =
	{	1
	,	{ 0, 0, 0 }
	,	{ p, 0, 0 }
	,	{ w, 0, 0 }
	};
	return v;
}
