#include	<stdio.h>
#include	<stdlib.h>
#include	<assert.h>
#include	<OpenCL/opencl.h>
 
#include	"PrimeKernel.cl.h"
 
#include	<JpMooParaiso/Generic.h>
using	namespace	JpMooParaiso;

#include	<OpenCL/opencl.h>
#include	"OpenCL.h"

typedef	cl_ushort		unit_t;
typedef	cl_uint			unitD_t;
typedef	cl_ulong		unitQ_t;

//	1,000,000,000 * log2(10) = 3,321,928,094.887362
//	( 3,321,928,095 +  7 ) /  8 = 415,241,012
//	( 3,321,928,095 + 31 ) / 32 = 103,810,253
//	( 3,321,928,095 + 63 ) / 64 =  51,905,127

//	#define	NUM_BITS_MAX			size_t( 3321928095LL )

//	100,000,000 * log2(10) = 332,192,809.4887362
//	( 332,192,810 +  7 ) /  8 = 41,524,102
//	( 332,192,810 + 31 ) / 32 = 10,381,026
//	( 332,192,810 + 63 ) / 64 =  5,190,513

//	#define	NUM_BITS_MAX			size_t( 332192810LL )
//	#define	NUM_BUFFER_BYTES	NUM_BITS
	//	NUM_BITS / 8 * 8 ( for squaring )

//	FED^2 = FDA169
//	FFEEDD^2 = FFDDBB25AAC9
//	https://developer.apple.com/library/mac/documentation/Performance/Conceptual/OpenCL_MacProgGuide/ExampleHelloWorld/Example_HelloWorld.html#//apple_ref/doc/uid/TP40008312-CH112-SW2

template	<typename T>	T
N( T p )
{	assert( p );
	return p;
}

template	<typename T>	struct
Buffer
{	T*		u;
	size_t	uSize;
	virtual	~
	Buffer()
	{	gcl_free( u );
	}
	Buffer( size_t p )
	:	u( N( (T*)gcl_malloc( sizeof( T ) * p, NULL, 0 ) ) )
	,	uSize( p )
	{
	}
	operator	T*()		{ return (T*)u; }
	T
	Get( size_t pOffset )
	{	T	v;
		size_t	wDstOrigin[] = { 0, 0, 0 };
		size_t	wSrcOrigin[] = { 0, 0, pOffset };
		size_t	wRegion[] = { sizeof( T ), 1, 1 };
		
		gcl_memcpy_rect
		(	&v
		,	u
		,	wDstOrigin
		,	wSrcOrigin
		,	wRegion
		,	0
		,	0
		,	0
		,	0
		);
		return v;
	}
	void
	Put( size_t pOffset, T const& p )
	{	size_t	wDstOrigin[] = { 0, 0, pOffset };
		size_t	wSrcOrigin[] = { 0, 0, 0 };
		size_t	wRegion[] = { sizeof( T ), 1, 1 };
		
		gcl_memcpy_rect
		(	u
		,	&p
		,	wDstOrigin
		,	wSrcOrigin
		,	wRegion
		,	0
		,	0
		,	0
		,	0
		);
	}
	void
	Add( T p, size_t pIndex = 0 )
	{	T w = Get( pIndex );
		Put( pIndex, w + p );
		if ( Get( pIndex ) < w )
		{	do
			{	w = Get( ++pIndex );
				Put( pIndex, ++w );
			} while ( ! w );
		}
	}
	void
	Sub( T p, size_t pIndex = 0 )
	{	T w = Get( pIndex );
		Put( pIndex, w - p );
		if ( Get( pIndex ) > w )
		{	do
			{	w = Get( ++pIndex );
				Put( pIndex, w - 1 );
			} while ( ! w );
		}
	}
};

struct
LLTester
{	bool				uResult;

			size_t		uNumBits;
	const	size_t		cNumUnits;
	const	size_t		cNumShifts;

	Buffer<unit_t>		u;
	Buffer<unit_t>		uC;		//	Carry
	Buffer<unitQ_t>		uS;		//	Square

	Buffer<bool>		uFlag;

	#define	NUM_BITS	( sizeof( unit_t ) * 8 )
	#define	NUM_Q_BITS	( sizeof( unitQ_t ) * 8 )
	 
	void
	Square()
	{	cl_ndrange wNDRange = { 1, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 } };

		wNDRange.global_work_size[ 0 ] = cNumUnits * 2 - 1;
		Square1st_kernel
		(	&wNDRange
		,	u
		,	uS
		,	cNumUnits
		);
		
		wNDRange.global_work_size[ 0 ] = cNumUnits;
		uFlag.Put( 0, false );
		Square2nd_kernel
		(	&wNDRange
		,	(unit_t*)(unitQ_t*)uS
		,	u
		,	uC
		,	uFlag
		);

		if ( uFlag.Get( 0 ) )
		{	size_t wOffset = 0;
			++wOffset;
			do
			{	wNDRange.global_work_size[ 0 ] = cNumUnits * 2 - wOffset;
				uFlag.Put( 0, false );
				Add_kernel
				(	&wNDRange
				,	u
				,	wOffset++
				,	uC
				,	uFlag
				);
			} while ( uFlag.Get( 0 ) );
		}
	}
	void
	Fold()
	{	cl_ndrange wNDRange = { 1, { 0, 0, 0 }, { cNumUnits, 0, 0 }, { 0, 0, 0 } };
		Fold_kernel( &wNDRange, u, uC, cNumShifts );
		{	{	unit_t w = u.Get( cNumUnits - 1 );
				u.Put( cNumUnits - 1, w & unit_t( -1 ) >> ( NUM_BITS - cNumShifts ) );
			}
			size_t wOffset = 0;
			do
			{	wNDRange.global_work_size[ 0 ] = cNumUnits - wOffset;
				uFlag.Put( 0, false );
				Add_kernel
				(	&wNDRange
				,	u
				,	wOffset++
				,	uC
				,	uFlag
				);
			} while ( uFlag.Get( 0 ) );

			{	unit_t w = u.Get( cNumUnits - 1 );
				unit_t wTest = unit_t( -1 ) << cNumShifts;
				if ( w & wTest )
				{	u.Put( cNumUnits - 1, w & ~ wTest );
					u.Add( 1 );
				}
			}
		}
	}
	bool
	Test()
	{	if ( cNumUnits > 1 )
		{	cl_ndrange wNDRange = { 1, { 0, 0, 0 }, { cNumUnits - 1, 0, 0 }, { 0, 0, 0 } };
			uFlag.Put( 0, false );
			Test_kernel( &wNDRange, u, uFlag );
			if ( uFlag.Get( 0 ) ) return false;	//	INV
		}
		return u.Get( cNumUnits - 1 ) == unit_t( ~ ( unit_t( -1 ) << cNumShifts ) );
	}
	void
	Zero()
	{	cl_ndrange wNDRange = { 1, { 0, 0, 0 }, { cNumUnits, 0, 0 }, { 0, 0, 0 } };
		Zero_kernel( &wNDRange, u );
	}
	
	LLTester( size_t pNumBits, int pDeviceType = CL_DEVICE_TYPE_CPU, cl_device_id pID = NULL )
	:	uNumBits	( pNumBits )
	,	cNumUnits	( ( pNumBits + NUM_BITS - 1 ) / NUM_BITS )
	,	cNumShifts	( uNumBits % NUM_BITS )
	,	u			( 2 * cNumUnits )
	,	uC			( 2 * cNumUnits )
	,	uS			( 2 * cNumUnits )
	,	uFlag		( sizeof( bool ) )
 	{
/*
	MacPro Late 2013		3.7GHz	Xeon	GPU
2014-03-01 21:10:26.443 OpenCL[573:303] 11213 is Prime Elappsed:12
2014-03-01 21:10:51.438 OpenCL[573:303] 19937 is Prime Elappsed:25
2014-03-01 21:11:46.488 OpenCL[573:303] 21701 is Prime Elappsed:55
2014-03-01 21:12:24.490 OpenCL[573:303] 23209 is Prime Elappsed:38
2014-03-01 21:17:33.589 OpenCL[573:303] 44497 is Prime Elappsed:309
	MacPro Late 2013		3.7GHz	Xeon	CPU
2014-03-01 21:19:10.507 OpenCL[608:303] 11213 is Prime Elappsed:3
2014-03-01 21:19:18.418 OpenCL[608:303] 19937 is Prime Elappsed:7
2014-03-01 21:19:27.524 OpenCL[608:303] 21701 is Prime Elappsed:9
2014-03-01 21:19:37.776 OpenCL[608:303] 23209 is Prime Elappsed:10
2014-03-01 21:20:16.912 OpenCL[608:303] 44497 is Prime Elappsed:39
2014-03-01 21:23:47.952 OpenCL[608:303] 86243 is Prime Elappsed:211
2014-03-01 21:30:48.741 OpenCL[608:303] 110503 is Prime Elappsed:420
2014-03-01 21:42:28.941 OpenCL[608:303] 132049 is Prime Elappsed:700
2014-03-01 22:30:53.810 OpenCL[608:303] 216091 is Prime Elappsed:2905
*/
		dispatch_sync
		(	N( gcl_create_dispatch_queue( pDeviceType, pID ) )	//	No need to release under ARC
		,	^{	Zero();
				u.Put( 0, 4 );
//bool	wTracing = false;
				for ( size_t i = 2; i < pNumBits; i++ )
				{	Square();
					u.Sub( 2 );
					Fold();
NSLog( @"%zu/%zu\n", i, pNumBits );
//Dump();
				}
				uResult = Test();
			}
		);
	}
//				NSLog( @"Elappesed(%llu)\n", wTimer.Milli() );
//				NSLog
//				(	@"2^%zu - 1 is %@\n"
//				,	p
//				,	wLLOK ? @"a prime --------------------" : @"not a prime"
//				);

	void
	Dump()
	{	printf( "uNumBits:%zu\n", uNumBits );
		size_t i = cNumUnits * 2;
		while ( i-- )
		{	unit_t w = u.Get( i );
			size_t j = sizeof( unit_t );
			while ( j-- ) printf( "%02x", ((char*)&w)[ j ] );
			printf( " " );
		}
		printf( "\n" );
	}
	void
	DumpCarry()
	{	printf( "uNumBits:%zu\n", uNumBits );
		size_t i = cNumUnits * 2;
		while ( i-- )
		{	unit_t w = uC.Get( i );
			size_t j = sizeof( unit_t );
			while ( j-- ) printf( "%02x", ((char*)&w)[ j ] );
			printf( " " );
		}
		printf( "\n" );
	}
	void
	DumpSquared()
	{	printf( "cNumUnits:%zu\n", cNumUnits );
		size_t i = cNumUnits * 2;
		while ( i-- )
		{	unit_t w = uS.Get( i );
			size_t j = sizeof( unitQ_t );
			while ( j-- ) printf( "%02x", ((char*)&w)[ j ] );
			printf( " " );
		}
		printf( "\n" );
	}
};

#import "AppDelegate.h"

@implementation AppDelegate

-	(void)
applicationDidFinishLaunching:(NSNotification*)p
{	NSLog( @"Start" );
	uint64_t sMs[] =
	{	3
	,	5
	,	7
	,	13
	,	17
	,	19
	,	31
	,	61
	,	89
	,	107
	,	127
	,	521
	,	607
	,	1279
	,	2203
	,	2281
	,	3217
	,	4253
	,	4423
	,	9689
	,	9941
	,	11213
	,	19937
	,	21701
	,	23209
	,	44497
	,	86243
	,	110503
	,	132049
	,	216091
	,	756839
	,	859433
	,	1257787
	,	1398269
	,	2976221
	,	3021377
	,	6972593
	,	13466917
	,	20996100
	,	24036583
	,	25964951
	,	30402457
	,	32582657
	,	37156667
	,	42643801
	,	43112609
	,	57885161
	};
//	printf( "lucas( 44497 ):%s\n", lucas( 44497 ) ? "Prime" : "Composite" );
//	printf( "Inspecting %llu is prime or not.\n", wPrimes[ i ] );
	Dump();
//	for ( int i = 0; i < 48; i++ )
	for ( int i = 30; i < 48; i++ )
	{
//		lucas( (int)sMs[ i ] );
		Timer wTimer;
		LLTester wLLTester( sMs[ i ] );
//		assert( wLLTester.uResult );
		NSLog( @"%llu is %@ Elappsed:%llu", sMs[ i ], wLLTester.uResult ? @"Prime" : @"Composite", wTimer.Second() );
	}
}

@end


/*
MacBook Air 11inch 2013 late using CPU
NumPlatforms：1
	Platform:Apple
	NumDevices:2
		ID:ffffffff	Device:Intel(R) Core(TM) i5-4250U CPU @ 1.30GHz
		Max units:4
		Max dimensions:3
		Max group size:1024
			Work item size:1024
			Work item size:1
			Work item size:1

		ID:1024500	Device:HD Graphics 5000
		Max units:280
		Max dimensions:3
		Max group size:512
			Work item size:512
			Work item size:512
			Work item size:512

NUM_BUFFER_BYTES:332192810
Elappesed(16)
2^3 - 1 is a prime --------------------
Elappesed(0)
2^5 - 1 is a prime --------------------
Elappesed(0)
2^7 - 1 is a prime --------------------
Elappesed(1)
2^13 - 1 is a prime --------------------
Elappesed(1)
2^17 - 1 is a prime --------------------
Elappesed(2)
2^19 - 1 is a prime --------------------
Elappesed(3)
2^31 - 1 is a prime --------------------
Elappesed(7)
2^61 - 1 is a prime --------------------
Elappesed(12)
2^89 - 1 is a prime --------------------
Elappesed(14)
2^107 - 1 is a prime --------------------
Elappesed(22)
2^127 - 1 is a prime --------------------
Elappesed(123)
2^521 - 1 is a prime --------------------
Elappesed(143)
2^607 - 1 is a prime --------------------
Elappesed(292)
2^1279 - 1 is a prime --------------------
Elappesed(521)
2^2203 - 1 is a prime --------------------
Elappesed(533)
2^2281 - 1 is a prime --------------------
Elappesed(944)
2^3217 - 1 is a prime --------------------
Elappesed(1108)
2^4253 - 1 is a prime --------------------
Elappesed(1141)
2^4423 - 1 is a prime --------------------
Elappesed(3542)
2^9689 - 1 is a prime --------------------
Elappesed(3670)
2^9941 - 1 is a prime --------------------
Elappesed(4500)
2^11213 - 1 is a prime --------------------
Elappesed(13644)
2^19937 - 1 is a prime --------------------
Elappesed(17871)
2^21701 - 1 is a prime --------------------
Elappesed(18931)
2^23209 - 1 is a prime --------------------
Elappesed(94536)
2^44497 - 1 is a prime --------------------
*/
//	void
//	AdjustNumBits()
//	{	while ( mNumBits )
//		{	uint64_t w = m.Get<uint64_t>( ( mNumBits - 1 ) / 64 );
//			uint64_t wMask = uint64_t( 1 ) << ( ( mNumBits - 1 ) % 64 );
//			if ( w & wMask ) break;
//			mNumBits--;
//		}
//	}
//	
//	inline	size_t
//	NumUnit1sC()
//	{	return ( mNumBits + NumUnit1Bits() - 1 ) / NumUnit1Bits();
//	}
//	
//	inline	size_t
//	NumUnit2sC()
//	{	return ( mNumBits + NumUnit2Bits() - 1 ) / NumUnit2Bits();
//	}
//	
//	inline	size_t
//	NumUnit4sC()
//	{	return ( mNumBits + NumUnit4Bits() - 1 ) / NumUnit4Bits();
//	}

//	void
//	Dump()
//	{	printf( ":%zu\n", mNumBits );
//
//		size_t wIndex = 16;
//		unit1_t w[ wIndex ];
//		gcl_memcpy( w, m.m, sizeof( w ) );
//		while( wIndex-- ) printf( "%08x ", w[ wIndex ] );
//		printf( "\n\n" );
//	}
//
//	void
//	DumpW()
//	{	printf( "------------------------------\n" );
//
//		size_t wIndex = 16;
//		unit1_t w[ wIndex ];
//		gcl_memcpy( w, mW.m, sizeof( w ) );
//#ifdef	CPU
//		while( wIndex-- ) printf( "%08x ", w[ wIndex ] );
//#endif
//#ifdef	GPU
//		while( wIndex-- ) printf( "%08llx %08llx %08llx %08llx ", w[ wIndex ].s[ 0 ], w[ wIndex ].s[ 1 ], w[ wIndex ].s[ 2 ], w[ wIndex ].s[ 3 ] );
//#endif
//		printf( "\n\n" );
//	}
//
//
//#define NUM_VALUES 1
//
//void
//Test()
//{
//    int i;
//    char name[128];
// 
//    dispatch_queue_t wQ = gcl_create_dispatch_queue(CL_DEVICE_TYPE_GPU, NULL);
// 
//    if ( ! wQ ) wQ = gcl_create_dispatch_queue(CL_DEVICE_TYPE_CPU, NULL);
// 
//    cl_device_id gpu = gcl_get_device_id_with_dispatch_queue( wQ );
//    clGetDeviceInfo(gpu, CL_DEVICE_NAME, 128, name, NULL);
//    fprintf(stdout, "Created a dispatch queue using the %s\n", name);
// 
//    cl_ushort* test_in = (cl_ushort*)malloc(sizeof(cl_ushort) * NUM_VALUES);
//	cl_ulong* test_io = (cl_ulong*)malloc(sizeof(cl_ulong) * NUM_VALUES);
//	for (i = 0; i < NUM_VALUES; i++) {
//        test_in[ i ] = 1024 - i;
//    }
// 
//    cl_ushort* mem_in  = (cl_ushort*)gcl_malloc(sizeof(cl_ushort) * NUM_VALUES, test_in, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR);
//    cl_ulong* mem_io = (cl_ulong*)gcl_malloc(sizeof(cl_ulong) * NUM_VALUES, NULL, 0);
// 
//    dispatch_sync
//	(	wQ
//	,	^{
//			cl_ndrange range = {                                              // 6
//				1,
//				{0, 0, 0},
//				{2-1, 0, 0},
//				{0, 0, 0}
//			};
//			Square1st_kernel(&range,(cl_ushort*)mem_in, (cl_ulong*)mem_io,1);
//			gcl_memcpy(test_io, mem_io, sizeof( cl_ulong ) * NUM_VALUES);
//    	}
//	);
//// 
////    for ( int i = 0; i < NUM_VALUES; i++ )
////	{	assert( ((cl_ushort*)test_io)[ i ] == 1024 );
////		assert( ((cl_ushort*)test_in)[ i ] == 0 );
////	}
//	printf( "Check OK\n" );
//    // Don't forget to free up the CL device's memory when you're done. // 10
//    gcl_free(mem_in);
//    gcl_free(mem_io);
// 
//    // And the same goes for system memory, as usual.
//    free(test_in);
//    free(test_io);
// 
//    // Finally, release your queue just as you would any GCD queue.    // 11
////    dispatch_release(queue);
//}

