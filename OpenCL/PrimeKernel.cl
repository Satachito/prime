typedef	ushort	unit_t;
typedef	uint	unitD_t;
typedef	ulong	unitQ_t;

kernel	void
Zero
(	global	unit_t*	p
)
{	p[ get_global_id( 0 ) ] = 0;
}

kernel	void
Add
(	global	unit_t*	p
,			size_t	pOffset
,	global	unit_t*	pCarry
,	global	bool*	pHasCarry
)
{	size_t	i = get_global_id( 0 );
	p[ i + pOffset ] += pCarry[ i ];
	if ( p[ i + pOffset ] < pCarry[ i ] )
	{	pCarry[ i ] = 1;
		*pHasCarry = true;
	}
	else pCarry[ i ] = 0;
}

//						  5	  4	  3	  2	  1	  0
//						  5	  4	  3	  2	  1	  0
//---------------------------------------------
//	 55		 44		 33		 22		 11		 00
//		 54	 53	 52	 51	 50
//				 43	 42	 41	 40
//						 32	 31	 30
//								 21	 20
//										 10

kernel	void
Square1st
(	global	unit_t*		pI
,	global	unitQ_t*	pO
,			size_t		pNumC
)
{
	size_t	i = get_global_id( 0 );

	size_t	wS;
	size_t	wE;
	if ( i < pNumC )
	{	wS = 0;
		wE = i;
	}
	else
	{	wS = i - pNumC + 1;
		wE = pNumC - 1;
	}
	unitQ_t v = 0;
	while ( wS < wE ) v += unitQ_t( pI[ wS++ ] ) * unitQ_t( pI[ wE-- ] );
	v *= 2;
	if ( wS == wE ) v += unitQ_t( pI[ wS ] ) * unitQ_t( pI[ wE ] );
	pO[ i ] = v;
}




//  13  12  11  10   9   8   7   6   5   4   3   2   1   0
//--------------------------------------------------------
//                                   5   4   3   2   1   0
//                                   5   4   3   2   1   0
//--------------------------------------------------------
//              40  36  32  28  24  20  16  12   8   4   0
//          41  37  33  29  25  21  17  13   9   5   1
//          38  34  30  26  22  18  14  10   6   2
//          35  31  27  23  19  15  11   7   3
//--------------------------------------------------------

//                                                       0
//                                                       0
//--------------------------------------------------------
//                                                       0
//                                                   1
//--------------------------------------------------------

//                                                   1   0
//                                                   1   0
//--------------------------------------------------------
//                                               8   4   0
//                                           9   5   1
//                                           6   2
//                                           3
//--------------------------------------------------------

//                                               2   1   0
//                                               2   1   0
//--------------------------------------------------------
//                                      16  12   8   4   0
//                                  17  13   9   5   1
//                                  14  10   6   2
//                                  11   7   3


kernel	void
Square2nd
(	global	unit_t*	pI
,	global	unit_t*	pO
,	global	unit_t*	pC
,	global	bool*	pHasCarry
)
{	size_t	i = get_global_id( 0 );
	size_t	wSize = get_global_size( 0 );
	switch ( wSize )
	{
	case 0:
		break;
	case 1:
		pO[ 0 ] = pI[ 0 ];
		pC[ 0 ] = 0;
		pO[ 1 ] = pI[ 1 ];
		pC[ 1 ] = 0;
		break;
	default:
		{	unitD_t	w;
			
			switch ( i )
			{
			case 0:
				pO[ 0 ] = pI[ 0 ];
				pC[ 0 ] = 0;

				w
				=	(unitD_t)pI[ 1 ]
				+	(unitD_t)pI[ 4 ]
				;
				pO[ 1 ] = *(unit_t*)&w;
				if ( ( pC[ 1 ] = *( (unit_t*)&w + 1 ) ) ) *pHasCarry = true;

				break;
			case 1:
				w
				=	(unitD_t)pI[ 2 ]
				+	(unitD_t)pI[ 5 ]
				+	(unitD_t)pI[ 8 ]
				;
				pO[ 2 ] = *(unit_t*)&w;
				if ( ( pC[ 2 ] = *( (unit_t*)&w + 1 ) ) ) *pHasCarry = true;

				w
				=	(unitD_t)pI[ wSize * 8 - 13 ]
				+	(unitD_t)pI[ wSize * 8 - 10 ]
				+	(unitD_t)pI[ wSize * 8 -  7 ]
				;
				pO[ wSize * 2 - 1 ] = *(unit_t*)&w;
				if ( ( pC[ wSize * 2 - 1 ] = *( (unit_t*)&w + 1 ) ) ) *pHasCarry = true;
				
				break;
			default:
				w
				=	(unitD_t)pI[ i * 8 - 13 ]
				+	(unitD_t)pI[ i * 8 - 10 ]
				+	(unitD_t)pI[ i * 8 -  7 ]
				+	(unitD_t)pI[ i * 8 -  4 ]
				;
				pO[ i * 2 - 1 ] = *(unit_t*)&w;
				if ( ( pC[ i * 2 - 1 ] = *( (unit_t*)&w + 1 ) ) ) *pHasCarry = true;

				w
				=	(unitD_t)pI[ i * 8 -  9 ]
				+	(unitD_t)pI[ i * 8 -  6 ]
				+	(unitD_t)pI[ i * 8 -  3 ]
				+	(unitD_t)pI[ i * 8 -  0 ]
				;
				pO[ i * 2 ] = *(unit_t*)&w;
				if ( ( pC[ i * 2 ] = *( (unit_t*)&w + 1 ) ) ) *pHasCarry = true;
				break;
			}
		}
		break;
	}
}

kernel	void
Fold
(	global	unit_t*	pI
,	global	unit_t*	pO
,			size_t	pNumShifts
)
{	size_t	i = get_global_id( 0 );
	size_t	wSize = get_global_size( 0 );
	pO[ i ]
	=	( pI[ i + wSize - 1 ] >> pNumShifts )
	|	( pI[ i + wSize ] << sizeof( unit_t ) * 8 - pNumShifts )
	;
}

kernel	void
Test
(	global	unit_t*	p
,	global	bool*	pFail
)
{	size_t	i = get_global_id( 0 );
	if ( p[ i ] != unit_t( -1 ) ) *pFail = true;
}

