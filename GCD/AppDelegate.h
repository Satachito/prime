//
//  AppDelegate.h
//  GCD
//
//  Created by Satoru Ogura on 2014/02/27.
//  Copyright (c) 2014年 Satoru Ogura. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>

@property (assign) IBOutlet NSWindow *window;

@end
