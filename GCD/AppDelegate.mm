#import			"AppDelegate.h"

#include		<JpMooParaiso/Exceptions.h>
#include		<JpMooParaiso/Generic.h>
using namespace	JpMooParaiso;

typedef	uint64_t	prime_t;

template<prime_t N>	struct
PrimeTable
{	uint8_t*	u;
	~
	PrimeTable()
	{	delete[] u;
	}
	PrimeTable()
	:	u( new uint8_t[ N / 8 ] )
	{	printf( "Making primes to %llu\n", N );
		assert( N % 8 == 0 );
		JpMooParaiso::Zero( u, N / 8 );
		for ( uint64_t i = 2; i < N; i++ )
		{	if ( ! ( u[ i / 8 ] & ( 1 << i % 8 ) ) )
			{	for ( uint64_t j = i + i; j < N; j += i )
				{	u[ j / 8 ] |= ( 1 << j % 8 );
				}
			}
		}
	}
	prime_t
	Max() const
	{	return N;
	}
	size_t
	NumPrimes() const
	{	size_t	v = 0;
		for ( uint64_t i = 2; i < N; i++ )
		{	if ( ! ( u[ i / 8 ] & ( 1 << i % 8 ) ) ) ++v;
		}
		return v;
	}
//	prime_t
//	Prime( uint64_t p ) const
//	{	uint64_t	w = 0;
//		for ( uint64_t i = 2; i < N; i++ )
//		{	if ( ! ( u[ i / 8 ] & ( 1 << i % 8 ) ) )
//			{	if ( w++ == p ) return i;
//			}
//		}
//		assert( false );
//	}
};

template<prime_t N>	struct
PrimeStream : iStream<prime_t>
{	PrimeTable<N> const&	u;
	prime_t					uIndex;
	PrimeStream( PrimeTable<N> const& p )
	:	u( p )
	,	uIndex( 1 )
	{
	}
	bool
	Avail()
	{	while ( ++uIndex < u.Max() )
		{	if ( ! ( u.u[ uIndex / 8 ] & ( 1 << uIndex % 8 ) ) ) return true;
		}
		return false;
	}
	operator
	prime_t() const
	{	return uIndex;
	}
};


//	T must be unsigned.
template	< typename T >	inline	void
Add( T* pDist, T p )
{	T w = *pDist;
	*pDist += p;
	while ( *pDist < w ) w = (*++pDist)++;
}

//	T must be unsigned.
template	< typename T >	inline	void
Sub( T* pDist, T p )
{	T w = *pDist;
	*pDist -= p;
	while ( *pDist > w ) w = (*++pDist)--;
}

template	< typename T >	inline	bool
Bit( T* p, size_t pBitIndex )
{	static	T	sOne = 1;
	return	p[ pBitIndex / ( sizeof( T ) * 8 ) ] & ( sOne << ( pBitIndex % ( sizeof( T ) * 8 ) ) );
}
template	< typename T >	inline	void
On( T* p, size_t pBitIndex )
{	static	T	sOne = 1;
			p[ pBitIndex / ( sizeof( T ) * 8 ) ] |= ( sOne << ( pBitIndex % ( sizeof( T ) * 8 ) ) );
}
template	< typename T >	inline	void
Off( T* p, size_t pBitIndex )
{	static	T	sOne = 1;
			p[ pBitIndex / ( sizeof( T ) * 8 ) ] &= ~ ( sOne << ( pBitIndex % ( sizeof( T ) * 8 ) ) );
}

//	All types must be unsigned.
//template < typename T, typename QUAD_T > struct

typedef	__uint8_t	unit_t;
typedef	__uint16_t	unitD_t;
typedef	__uint32_t	unitQ_t;

//typedef	__uint32_t	unit_t;
//typedef	__uint64_t	unitD_t;
//typedef	__uint128_t	unitQ_t;

struct
LLTester
{			size_t		uNumBits;

	const	size_t		cNumUnits;

			unit_t*		u;
			unit_t*		uC;
			unitQ_t*	uQ;

	const	size_t		cNumShifts;
				
	#define	NUM_BITS	( sizeof( unit_t ) * 8 )
	#define	NUM_Q_BITS	( sizeof( unitQ_t ) * 8 )
	 
	~
	LLTester()
	{	delete[] u;
		delete[] uQ;
	}
	LLTester( size_t pNumBits = 0 )
	:	uNumBits( pNumBits )
	,	cNumUnits( ( pNumBits + NUM_BITS - 1 ) / NUM_BITS )
	,	u( new unit_t[ cNumUnits * 2 ] )
	,	uC( new unit_t[ cNumUnits * 2 ] )
	,	uQ( new unitQ_t[ cNumUnits * 2 ] )
	,	cNumShifts( uNumBits % NUM_BITS )
	{	assert( NUM_BITS * 4 == NUM_Q_BITS );

		JpMooParaiso::Zero( u, cNumUnits );
		u[ 0 ] = 4;
bool wTrace = false;
		for ( size_t i = 2; i < uNumBits; i++ )
		{
//	Square
			switch ( cNumUnits )
			{
			case 0:
				assert( cNumUnits );
				break;
			case 1:
				*(unitQ_t*)u = (unitQ_t)*u * (unitQ_t)*u;
				break;
			case 2:
				*(unitQ_t*)u = (unitQ_t)*(unitD_t*)u * (unitQ_t)*(unitD_t*)u;
				break;
			default:
				{	dispatch_group_t wG = ::dispatch_group_create();
					dispatch_queue_t wQ = ::dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0 );
					for ( size_t j = 0; j < cNumUnits; j++ ) dispatch_group_async
					(	wG
					,	wQ
					,	^{	unit_t*	wS = u;
							unit_t*	wE = u + j;
							unitQ_t	w = 0;
							while ( wS < wE ) w += (unitQ_t)*wS++ * (unitQ_t)*wE--;
							w <<= 1;
							if ( wS == wE ) w += (unitQ_t)*wS * (unitQ_t)*wE;
							uQ[ j ] = w;
						}
					);
					for ( size_t j = cNumUnits; j < cNumUnits * 2 - 1; j++ ) dispatch_group_async
					(	wG
					,	wQ
					,	^{	unit_t*	wS = u + j - cNumUnits + 1;
							unit_t*	wE = u + cNumUnits - 1;
							unitQ_t	w = 0;
							while ( wS < wE ) w += (unitQ_t)*wS++ * (unitQ_t)*wE--;
							w <<= 1;
							if ( wS == wE ) w += (unitQ_t)*wS * (unitQ_t)*wE;
							uQ[ j ] = w;
						}
					);

					dispatch_group_wait( wG, DISPATCH_TIME_FOREVER );

					{
						__block	bool	wHasCarry;
							
						unit_t*	wS = (unit_t*)uQ;		//	Squared
//	0
						u[ 0 ] = wS[ 0 ];
						uC[ 0 ] = 0;
//	1
						{	unitD_t w
							=	(unitD_t)wS[ 1 ]
							+	(unitD_t)wS[ 4 ]
							;
							u[ 1 ] = (unit_t)w;
							if ( ( uC[ 1 ] = *( (unit_t*)&w + 1 ) ) ) wHasCarry = true;
//	u[ cNumUnits * 2 ] and u[ cNumUnits * 2 + 1 ] must be 0.
//	If set, carry informations will be vanished.
//	2
						}
						{	unitD_t w
							=	(unitD_t)wS[ 2 ]
							+	(unitD_t)wS[ 5 ]
							+	(unitD_t)wS[ 8 ]
							;
							u[ 2 ] = (unit_t)w;
							if ( ( uC[ 2 ] = *( (unit_t*)&w + 1 ) ) ) wHasCarry = true;
						}
//	cNumUnits * 2 - 1
						{	unitD_t w
							=	(unitD_t)wS[ cNumUnits * 8 - 13 ]
							+	(unitD_t)wS[ cNumUnits * 8 - 10 ]
							+	(unitD_t)wS[ cNumUnits * 8 -  7 ]
							;
							u[ cNumUnits * 2 - 1 ] = (unit_t)w;
							if ( ( uC[ cNumUnits * 2 - 1 ] = *( (unit_t*)&w + 1 ) ) ) wHasCarry = true;
						}
//	3 ~ cNumUnits * 2 - 2
						for ( size_t j = 2; j < cNumUnits; j++ ) dispatch_group_async
						(	wG
						,	wQ
						,	^{	unitD_t w
								=	(unitD_t)wS[ j * 8 - 13 ]
								+	(unitD_t)wS[ j * 8 - 10 ]
								+	(unitD_t)wS[ j * 8 -  7 ]
								+	(unitD_t)wS[ j * 8 -  4 ]
								;
								u[ j * 2 - 1 ] = (unit_t)w;
								if ( ( uC[ j * 2 - 1 ] = *((unit_t*)&w + 1 ) ) ) wHasCarry = true;

								w
								=	(unitD_t)wS[ j * 8 -  9 ]
								+	(unitD_t)wS[ j * 8 -  6 ]
								+	(unitD_t)wS[ j * 8 -  3 ]
								+	(unitD_t)wS[ j * 8 -  0 ]
								;
								u[ j * 2 ] = (unit_t)w;
								if ( ( uC[ j * 2 ] = *((unit_t*)&w + 1 ) ) ) wHasCarry = true;
							}
						);
						dispatch_group_wait( wG, DISPATCH_TIME_FOREVER );
						
						if ( wHasCarry )
						{	size_t	wCarryOffset = 0;
							do
							{	wHasCarry = false;
								++wCarryOffset;
								for ( size_t j = wCarryOffset; j < cNumUnits * 2; j++ ) dispatch_group_async
								(	wG
								,	wQ
								,	^{	u[ j ] += uC[ j - wCarryOffset ];
										if ( u[ j ] < uC[ j - wCarryOffset ] )
										{	uC[ j - wCarryOffset ] = 1;
											wHasCarry = true;
										}
										else uC[ j - wCarryOffset ] = 0;
									}
								);
								dispatch_group_wait( wG, DISPATCH_TIME_FOREVER );
							} while ( wHasCarry );
						}
					}
				}
				break;
			}
			Sub( u, (unit_t)2 );
//if ( wTrace )
//{	printf( "After Square - 2\n" );
//	DumpD();
//}
			for ( size_t j = 0; j < cNumUnits; j++ )
			{	uC[ j ]
				=	( u[ j + cNumUnits - 1 ] >> cNumShifts )
				|	( u[ j + cNumUnits ] << ( NUM_BITS - cNumShifts ) )
				;
			}
			u[ cNumUnits - 1 ] &= unit_t( -1 ) >> ( NUM_BITS - cNumShifts );
			for ( size_t i = 0; i < cNumUnits; i++ ) Add( u + i, uC[ i ] );
//if ( wTrace )
//{	printf( "Folding\n" );
//	Dump();
//	DumpQ();
//	printf( "\n" );
//}
			if ( Bit( u, uNumBits ) )
			{	Off( u, uNumBits );
				Add( u, (unit_t)1 );
			}
			assert( ! Bit( u, uNumBits ) );
if ( wTrace )
{	Dump();
	printf( "\n" );
}
//if ( u[ 0 ] == 0xfe ) wTrace = true;
		}
	}

	bool
	Test()
	{	for ( int i = 0; i < cNumUnits - 1; i++ ) if ( u[ i ] != unit_t( -1 ) ) return false;
		return u[ cNumUnits - 1 ] == unit_t( ~ ( unit_t( -1 ) << cNumShifts ) );
	}
	
	void
	Dump()
	{	printf( "uNumBits:%zu\n", uNumBits );
		size_t i = cNumUnits;
		while ( i-- )
		{	uint8_t* w = (uint8_t*)&u[ i ];
			size_t j = sizeof( unit_t );
			while ( j-- ) printf( "%02x", w[ j ] );
			printf( " " );
		}
		printf( "\n" );
	}
	void
	DumpD()
	{	printf( "uNumBits:%zu\n", uNumBits );
		size_t i = cNumUnits * 2;
		while ( i-- )
		{	uint8_t* w = (uint8_t*)&u[ i ];
			size_t j = sizeof( unit_t );
			while ( j-- ) printf( "%02x", w[ j ] );
			printf( " " );
		}
		printf( "\n" );
	}
	void
	DumpC()
	{	size_t i = cNumUnits * 2;
		while ( i-- )
		{	uint8_t* w = (uint8_t*)&u[ i + cNumUnits * 2 ];
			size_t j = sizeof( unit_t );
			while ( j-- ) printf( "%02x", w[ j ] );
			printf( " " );
		}
		printf( "\n" );
	}
	void
	DumpQ()
	{	printf( "cNumUnits:%zu\n", cNumUnits );
		size_t i = cNumUnits * 2;
		while ( i-- )
		{	uint8_t* w = (uint8_t*)&uQ[ i ];
			size_t j = 4 * sizeof( unit_t );
			while ( j-- ) printf( "%02x", w[ j ] );
			printf( " " );
		}
		printf( "\n" );
	}
};


//						  5	  4	  3	  2	  1	  0
//						  5	  4	  3	  2	  1	  0
//---------------------------------------------
//	 55		 44		 33		 22		 11		 00
//		 54	 53	 52	 51	 50
//				 43	 42	 41	 40
//						 32	 31	 30
//								 21	 20
//										 10

/*
21 : 11213 is Prime  Elappesed(8)
22 : 19937 is Prime  Elappesed(25)
23 : 21701 is Prime  Elappesed(32)
24 : 23209 is Prime  Elappesed(38)
25 : 44497 is Prime  Elappesed(131)
26 : 86243 is Prime  Elappesed(453)
*/


//	1,000,000,000 * log2(10) = 3,321,928,094.887362
//	( 3,321,928,095 +  7 ) /  8 = 415,241,012
//	( 3,321,928,095 + 31 ) / 32 = 103,810,253
//	( 3,321,928,095 + 63 ) / 64 =  51,905,127

//	#define	NUM_DIGITS	3321928095

//	100,000,000 * log2(10) = 332,192,809.4887362
//	( 332,192,810 +  7 ) /  8 = 41,524,102
//	( 332,192,810 + 31 ) / 32 = 10,381,026
//	( 332,192,810 + 63 ) / 64 =  5,190,513

	#define	NUM_DIGITS	332192810LL

#include	"Lucas.h"

void
Test()
{
	__uint128_t wPrimes[] =
	{	3
	,	5
	,	7
	,	13
	,	17
	,	19
	,	31
	,	61
	,	89
	,	107
	,	127
	,	521
	,	607
	,	1279
	,	2203
	,	2281
	,	3217
	,	4253
	,	4423
	,	9689
	,	9941
	,	11213
	,	19937
	,	21701
	,	23209
	,	44497
	,	86243
	,	110503
	,	132049
	,	216091
	,	756839
	,	859433
	,	1257787
	,	1398269
	,	2976221
	,	3021377
	,	6972593
	,	13466917
	,	20996100
	,	24036583
	,	25964951
	,	30402457
	,	32582657
	,	37156667
	,	42643801
	,	43112609
	,	57885161
	};
//	for ( int i = 0; i < 48; i++ )
	for ( int i = 0; i < 48; i++ )
	{	Timer wTimer;
//		Lucas( wPrimes[ i ] );
		LLTester w( wPrimes[ i ] );
		NSLog( @"%d : %llu is %@ Elappesed(%llu)", i, (uint64_t)wPrimes[ i ], w.Test() ? @"Prime" : @"Composite", wTimer.Second() );
	}
}



@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
	Test();
}

@end
