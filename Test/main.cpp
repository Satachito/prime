#include <iostream>
#include <assert.h>
#include <OpenCL/OpenCL.h>
#include "Test.cl.h"

#include "OpenCL.h"

template	<typename T>	T
Get( T* pPtr, size_t pOffset )
{	T	v;
	size_t	wDstOrigin[] = { 0, 0, 0 };
	size_t	wSrcOrigin[] = { 0, 0, pOffset };
	size_t	wRegion[] = { sizeof( T ), 1, 1 };
 	
	gcl_memcpy_rect
	(	&v
	,	pPtr
	,	wDstOrigin
	,	wSrcOrigin
	,	wRegion
	,	0
	,	0
	,	0
	,	0
	);
	return v;
}
template	<typename T>	void
Put( T* pPtr, size_t pOffset, T const& p )
{	size_t	wDstOrigin[] = { 0, 0, pOffset };
	size_t	wSrcOrigin[] = { 0, 0, 0 };
	size_t	wRegion[] = { sizeof( T ), 1, 1 };
 	
	gcl_memcpy_rect
	(	pPtr
	,	&p
	,	wDstOrigin
	,	wSrcOrigin
	,	wRegion
	,	0
	,	0
	,	0
	,	0
	);
}

static	void
TestGlobalSize()
{	dispatch_queue_t	wQ = gcl_create_dispatch_queue( CL_DEVICE_TYPE_CPU, NULL );
	unsigned short*	w = (unsigned short*)gcl_malloc( 4096 * sizeof( unsigned short ), NULL, 0 );
	dispatch_sync
	(	wQ
	,	^{	cl_ndrange wNDRange = { 1, { 0, 0, 0 }, { 4096, 0, 0 }, { 0, 0, 0 } };
			Types_kernel( &wNDRange, w );
			for ( size_t i = 0; i < 4096; i++ ) printf( "%zu:%hu\n", i, Get( w, i ) );
		}
	);
	gcl_free( w );
}
//	Macbook Air late 2014
//
//			cl_ndrange wNDRange = { 1, { 0, 0, 0 }, { 256, 0, 0 }, { 0, 0, 0 } };
//	GPU では get_num_groups は 1		groupあたり 256
//	CPU では get_num_groups は 32		groupあたり 8
//
//			cl_ndrange wNDRange = { 1, { 0, 0, 0 }, { 512, 0, 0 }, { 0, 0, 0 } };
//	GPU では get_num_groups は 1		groupあたり 512
//	CPU では get_num_groups は 32		groupあたり 16
//
//			cl_ndrange wNDRange = { 1, { 0, 0, 0 }, { 1024, 0, 0 }, { 0, 0, 0 } };
//	GPU では get_num_groups は 2		groupあたり 512
//	CPU では get_num_groups は 32		groupあたり 32
//
//			cl_ndrange wNDRange = { 1, { 0, 0, 0 }, { 2048, 0, 0 }, { 0, 0, 0 } };
//	GPU では get_num_groups は 4		groupあたり 512
//	CPU では get_num_groups は 32		groupあたり 64
//
//			cl_ndrange wNDRange = { 1, { 0, 0, 0 }, { 4096, 0, 0 }, { 0, 0, 0 } };
//	GPU では get_num_groups は 8		groupあたり 512
//	CPU では get_num_groups は 32		groupあたり 128
//
//			cl_ndrange wNDRange = { 1, { 0, 0, 0 }, { 8192, 0, 0 }, { 0, 0, 0 } };
//	GPU では get_num_groups は 16		groupあたり 512
//	CPU では get_num_groups は 64		groupあたり 128
//
//			cl_ndrange wNDRange = { 1, { 0, 0, 0 }, { 16384, 0, 0 }, { 0, 0, 0 } };
//	GPU では get_num_groups は 32		groupあたり 512
//	CPU では get_num_groups は 128	groupあたり 128
//
//			cl_ndrange wNDRange = { 1, { 0, 0, 0 }, { 32768, 0, 0 }, { 0, 0, 0 } };
//	GPU では get_num_groups は 64		groupあたり 512
//	CPU では get_num_groups は 256	groupあたり 128


static	void
Test1024()
{	dispatch_queue_t	wQ = gcl_create_dispatch_queue( CL_DEVICE_TYPE_CPU, NULL );
	char	wPlaceHolder[ 256 ];
	for ( int i = 0; i < 256; i++ ) wPlaceHolder[ i ] = 0xaa;
//	void*	w = gcl_malloc( 1024 / 8 * 2, wPlaceHolder, CL_MEM_COPY_HOST_PTR );
	void*	w = gcl_malloc( 1024 / 8 * 2, wPlaceHolder, CL_MEM_COPY_HOST_PTR );
	dispatch_sync
	(	wQ
	,	^{	cl_ndrange wNDRange = { 1, { 0, 0, 0 }, { 1, 0, 0 }, { 0, 0, 0 } };
			Div2_kernel( &wNDRange, (cl_ulong16*)w );
			for ( size_t i = 0; i < 32; i++ ) printf( "%zu:%lx\n", i, Get( (unsigned long*)w, i ) );
		}
	);
	gcl_free( w );
}

//static	void
//Test1024Add()
//{	dispatch_queue_t	wQ = gcl_create_dispatch_queue( CL_DEVICE_TYPE_CPU, NULL );
//	char	wPlaceHolder[ 1024 / 8 * 2 ];
//	for ( int i = 0; i < sizeof( wPlaceHolder ); i++ ) wPlaceHolder[ i ] = 0;
//	wPlaceHolder[ 127 ] = 0x80;
//	void*	wL = gcl_malloc( 1024 / 8 * 2, wPlaceHolder, CL_MEM_COPY_HOST_PTR );
//	void*	wR = gcl_malloc( 1024 / 8 * 2, wPlaceHolder, CL_MEM_COPY_HOST_PTR );
//	void*	wO = gcl_malloc( 1024 / 8 * 2, NULL, 0 );
//	dispatch_sync
//	(	wQ
//	,	^{	cl_ndrange wNDRange = { 1, { 0, 0, 0 }, { 2, 0, 0 }, { 0, 0, 0 } };
//			Add_kernel( &wNDRange, (cl_ulong16*)wL, (cl_ulong16*)wR, (cl_ulong16*)wO );
//			for ( size_t i = 0; i < sizeof( wPlaceHolder ); i++ ) printf( "%02x ", Get( (uint8_t*)wO, i ) );
//			printf( "\n" );
//for ( size_t i = 0; i < sizeof( wPlaceHolder ); i++ ) printf( "%02x", Get( (uint8_t*)wL, sizeof( wPlaceHolder ) - 1 - i ) );
//printf( "\n" );
//		}
//	);
//	gcl_free( wL );
//	gcl_free( wR );
//	gcl_free( wO );
//}

static	void
ShowMemory( void* p, size_t pSize )
{	uint8_t* w = (uint8_t*)p;
	for ( size_t i = 0; i < pSize; i++ ) printf( "%02x ", w[ i ] );
	printf( "\n" );
}

static	void
Test128BitsInteger()
{	__uint128_t	wU128 = (__uint128_t)0xffffffffffffffff * (__uint128_t)0xffffffffffffffff;
	ShowMemory( &wU128, 128 / 8 );
}

int
main( int argc, const char * argv[] )
{
	Dump();
	Test128BitsInteger();
    return 0;
}

