#include <immintrin.h>

#include		<JpMooParaiso/Exceptions.h>
#include		<JpMooParaiso/Generic.h>
using namespace	JpMooParaiso;

#include <stdio.h>

int lucas(int p)
{
    char *l, *m;
    char *lp, *mp1, *mp2;
    int  i, j, k;
    int  ca, x;
    int  ret = 1;

    if (p <= 0) return 0;
    if (p <= 2) return 1;

    if ((l = (char *)malloc(p)) == NULL) return -1;
    if ((m = (char *)malloc(p)) == NULL) {
        free(l);
        return -1;
    }

    for (lp = l + p; lp != l; ) *--lp = 0;
    *(l+2) = 1;                                     /* L(1) = 4 */

	uint64_t wCount = 0;
    for (i = 2; i < p; i++) {
        for (lp = l + p, mp1 = m + p; lp != l; ) {
            *--mp1 = *--lp;
            *lp = 1;                        /* 2^p -1 mod 2^p -1 = 0 */
        }
        *(l+1) = 0;                                 /* L(i) = L(i) - 2 */
        for (mp1 = m, j = 0; j < p; j++) {
            if (*mp1++) {                           /* this bit is 1 */
                ca = 0;
                k = j;
                for (mp2 = m; mp2 != m + p; ) {     /* L(i) * L(i) */
                    x = *mp2++ + *(l+k) + ca;
                    *(l+k) = x & 1;
                    ca = x >> 1;
                    if (++k == p) k = 0;
                }
                if (ca) {
                    while (*(l+k)) {
                        *(l+k) = 0;
                        if (++k == p) k = 0;
                    }
                    *(l+k) = 1;
                }
            }
        }
{	uint64_t w = 0;
	for ( uint64_t i = 0; i < p; i++ )
	{	if ( ( i % 64 ) == ( p % 64 ) )
		{	printf( "%16llx ", w );
			w = 0;
		}
		if ( l[ p - 1 - i ] )
		{	w |= ( 1LL << ( ( p - 1 - i ) % 64 ) );
		}
	}
	if ( p % 64 ) printf( "%16llx ", w );
	printf( "    ------ %llu\n", wCount++ );
}
	}

    x = *l;
    for (lp = l + p; lp != l; )               /* L(p-1) == all 0 or 1 ? */
        if (*--lp != x) {
            ret = 0;
            break;
        }

    free(m);
    free(l);
    return ret;
}

template<uint64_t N>	struct
PrimeTable
{	uint8_t*	u;
	~
	PrimeTable()
	{	delete[] u;
	}
	PrimeTable()
	:	u( new uint8_t[ N / 8 ] )
	{	printf( "Making primes to %llu\n", N );
		assert( N % 8 == 0 );
		JpMooParaiso::Zero( u, N / 8 );
		for ( uint64_t i = 2; i < N; i++ )
		{	if ( ! ( u[ i / 8 ] & ( 1 << i % 8 ) ) )
			{	for ( uint64_t j = i + i; j < N; j += i )
				{	u[ j / 8 ] |= ( 1 << j % 8 );
				}
			}
		}
	}
	uint64_t
	Max() const
	{	return N;
	}
	size_t
	NumPrimes() const
	{	size_t	v = 0;
		for ( uint64_t i = 2; i < N; i++ )
		{	if ( ! ( u[ i / 8 ] & ( 1 << i % 8 ) ) ) ++v;
		}
		return v;
	}
//	uint64_t
//	Prime( uint64_t p ) const
//	{	uint64_t	w = 0;
//		for ( uint64_t i = 2; i < N; i++ )
//		{	if ( ! ( u[ i / 8 ] & ( 1 << i % 8 ) ) )
//			{	if ( w++ == p ) return i;
//			}
//		}
//		assert( false );
//	}
};

template<uint64_t N>	struct
PrimeStream : iStream<uint64_t>
{	PrimeTable<N> const&	u;
	uint64_t				uIndex;
	PrimeStream( PrimeTable<N> const& p )
	:	u( p )
	,	uIndex( 1 )
	{
	}
	bool
	Avail()
	{	while ( ++uIndex < u.Max() )
		{	if ( ! ( u.u[ uIndex / 8 ] & ( 1 << uIndex % 8 ) ) ) return true;
		}
		return false;
	}
	operator
	uint64_t() const
	{	return uIndex;
	}
};




//	T must be unsigned.
template	< typename T >	inline	void
Add( T* pDist, T p )
{	T w = *pDist;
	*pDist += p;
	while ( *pDist < w ) w = (*++pDist)++;
}

//	T must be unsigned.
template	< typename T >	inline	void
Sub( T* pDist, T p )
{	T w = *pDist;
	*pDist -= p;
	while ( *pDist > w ) w = (*++pDist)--;
}

template	< typename T >	inline	bool
Bit( T* p, size_t pBitIndex )
{	static	T	sOne = 1;
	return	p[ pBitIndex / ( sizeof( T ) * 8 ) ] & ( sOne << ( pBitIndex % ( sizeof( T ) * 8 ) ) );
}
template	< typename T >	inline	void
On( T* p, size_t pBitIndex )
{	static	T	sOne = 1;
			p[ pBitIndex / ( sizeof( T ) * 8 ) ] |= ( sOne << ( pBitIndex % ( sizeof( T ) * 8 ) ) );
}
template	< typename T >	inline	void
Off( T* p, size_t pBitIndex )
{	static	T	sOne = 1;
			p[ pBitIndex / ( sizeof( T ) * 8 ) ] &= ~ ( sOne << ( pBitIndex % ( sizeof( T ) * 8 ) ) );
}

//	All types must be unsigned.
template < typename QUAD_T, typename HALF_T, typename T, size_t NUM_BITS_TO_ALLOC > struct
Bits
{
	#define	NUM_UNIT_BITS			( 8 * sizeof( T ) )
	#define	ALL_BITS				( (T)-1 )
	#define	NUM_ELEMENTS( p )		( ( ( p ) + NUM_UNIT_BITS ) / NUM_UNIT_BITS )

	#define	NUM_HALF_BITS			( 8 * sizeof( HALF_T ) )
	#define	NUM_QUAD_BITS			( 8 * sizeof( QUAD_T ) )

	size_t	uNumBits;
	T*		u;

	T*		uTmp;

	~
	Bits()
	{	delete[] u;
		delete[] uTmp;
	}
	Bits( size_t pNumBits = 0 )
	:	uNumBits( pNumBits )
	,	u( N( new T[ NUM_ELEMENTS( NUM_BITS_TO_ALLOC ) ] ) )
	,	uTmp( N( new T[ NUM_ELEMENTS( NUM_BITS_TO_ALLOC ) ] ) )
	{
//#ifdef	DEBUG
//printf( "NUM_BITS_TO_ALLOC = %zu, allocated %zu elements\n", NUM_BITS_TO_ALLOC, NUM_ELEMENTS( NUM_BITS_TO_ALLOC ) );
//#endif
	}

//	void operator
//	+= ( T p )
//	{	Add( u, p );
//		while ( Bit( uNumBits ) ) uNumBits++;
//	}
//	void operator
//	-= ( T p )
//	{	Sub( u, p );
//		while ( uNumBits && ! Bit( uNumBits - 1 ) ) uNumBits--;
//	}

	bool
	Bit( size_t pBitIndex )
	{	return ::Bit( u, pBitIndex );
	}
	void
	On( size_t pBitIndex )
	{	::On( u, pBitIndex );
	}
	void
	Off( size_t pBitIndex )
	{	::Off( u, pBitIndex );
	}
	void
	Dump()
	{	switch ( sizeof( T ) )
		{
		case 4:
			{	printf( "%2zu:", uNumBits );
				size_t i = ( uNumBits + NUM_UNIT_BITS - 1 ) / NUM_UNIT_BITS;
				printf( "(%08x)", (uint32_t)u[ i ] );
				while ( i-- ) printf( "%08x ", (uint32_t)u[ i ] );
			}
			break;
		case 8:
			{	printf( "%2zu:", uNumBits );
				size_t i = ( uNumBits + NUM_UNIT_BITS - 1 ) / NUM_UNIT_BITS;
				printf( "(%016llx)", (uint64_t)u[ i ] );
				while ( i-- ) printf( "%016llx ", (uint64_t)u[ i ] );
			}
			break;
		}
	}
	
	void
	Fold( size_t pBitIndex )
	{	assert( pBitIndex < uNumBits );
		assert( pBitIndex % 2 );
		T*		wDst = u;
		size_t	wLastIndex = uNumBits / NUM_UNIT_BITS;
		size_t	wIndex = pBitIndex / NUM_UNIT_BITS;
		size_t	wNumShift = pBitIndex % NUM_UNIT_BITS;
		T		wCarry = u[ wIndex ] >> wNumShift;
		u[ wIndex++ ] &= ~ ( ALL_BITS << wNumShift );
		while ( wIndex < wLastIndex )
		{	T w = u[ wIndex ];
			u[ wIndex++ ] = 0;
			Add<T>( wDst++, wCarry | w << ( NUM_UNIT_BITS - wNumShift ) );
			wCarry = w >> wNumShift;
		}
		if ( wIndex * NUM_UNIT_BITS < uNumBits )
		{	T w = u[ wIndex ];
			u[ wIndex++ ] = 0;
			Add<T>( wDst++, wCarry | w << ( NUM_UNIT_BITS - wNumShift ) );
			Add<T>( wDst++, w >> wNumShift );
		}
		else
		{	Add( wDst++, wCarry );
		}
		uNumBits -= pBitIndex;
		if ( uNumBits < pBitIndex ) uNumBits = pBitIndex;
		uNumBits++;
		while ( uNumBits && ! Bit( uNumBits - 1 ) ) uNumBits--;
	}

//						  5	  4	  3	  2	  1	  0
//						  5	  4	  3	  2	  1	  0
//---------------------------------------------
//	 55		 44		 33		 22		 11		 00
//		 54	 53	 52	 51	 50
//				 43	 42	 41	 40
//						 32	 31	 30
//								 21	 20
//										 10

	void
	Square()
	{
/*
11213 is Prime  Elappesed(2)
19937 is Prime  Elappesed(11)
21701 is Prime  Elappesed(15)
23209 is Prime  Elappesed(18)
44497 is Prime  Elappesed(127)
86243 is Prime  Elappesed(908)
*/
		size_t	wNumUnits = ( uNumBits + ( NUM_QUAD_BITS - 1 ) ) / NUM_QUAD_BITS;
		JpMooParaiso::Zero( (QUAD_T*)uTmp, wNumUnits * 2 );
		QUAD_T*	wQU = (QUAD_T*)u;
		QUAD_T*	wQT = (QUAD_T*)uTmp;

		size_t wI = 0;
		while ( wI < wNumUnits )
		{	size_t	wS = 0;
			size_t	wE = wI;
			T		w = 0;
			while ( wS < wE )
			{	w += (T)wQU[ wS ] * (T)wQU[ wE ];
				wS++;
				wE--;
			}
			w <<= 1;
			if ( wS == wE ) w += (T)wQU[ wI / 2 ] * (T)wQU[ wI / 2 ];
			*(T*)(wQT + wI++) += w;
		}
		for ( size_t i = 1; i < wNumUnits; i++ )
		{	size_t	wS = i;
			size_t	wE = wNumUnits - 1;
			T		w = 0;
			while ( wS < wE )
			{	w += (T)wQU[ wS ] * (T)wQU[ wE ];
				wS++;
				wE--;
			}
			w <<= 1;
			if ( wS == wE ) w += (T)wQU[ wI / 2 ] * (T)wQU[ wI / 2 ];
			*(T*)(wQT + wI++) += w;
		}

		T* wSwap = u;
		u = uTmp;
		uTmp = wSwap;
		uNumBits *= 2;
		while (	! Bit( uNumBits - 1 ) ) uNumBits--;
	}


	void
	SquareAsync()
	{
/*
	MacBook Pro Mid 2012	2.3GHz	i7
21 : 11213 is Prime  Elappesed(8)
22 : 19937 is Prime  Elappesed(25)
23 : 21701 is Prime  Elappesed(32)
24 : 23209 is Prime  Elappesed(38)
25 : 44497 is Prime  Elappesed(131)
26 : 86243 is Prime  Elappesed(453)
	MacPro Late 2013		3.7GHz	Xeon
21 : 11213 is Prime  Elappesed(7)
22 : 19937 is Prime  Elappesed(22)
23 : 21701 is Prime  Elappesed(26)
24 : 23209 is Prime  Elappesed(30)
25 : 44497 is Prime  Elappesed(105)
26 : 86243 is Prime  Elappesed(367)
27 : 110503 is Prime  Elappesed(643)
28 : 132049 is Prime  Elappesed(989)
29 : 216091 is Prime  Elappesed(3603)
*/
		size_t	wNumUnits = ( uNumBits + ( NUM_QUAD_BITS - 1 ) ) / NUM_QUAD_BITS;
		QUAD_T*	wQU = (QUAD_T*)u;

		dispatch_group_t wG = ::dispatch_group_create();
		dispatch_queue_t wQ = ::dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0 );

		size_t wI = 0;
		while ( wI < wNumUnits )
		{	dispatch_group_async
			(	wG
			,	wQ
			,	^{	size_t	wS = 0;
					size_t	wE = wI;
					T		w = 0;
					while ( wS < wE )
					{	w += (T)wQU[ wS ] * (T)wQU[ wE ];
						wS++;
						wE--;
					}
					w <<= 1;
					if ( wS == wE ) w += (T)wQU[ wI / 2 ] * (T)wQU[ wI / 2 ];
					uTmp[ wI ] = w;
				}
			);
			wI++;
		}
		for ( size_t i = 1; i < wNumUnits; i++ )
		{	dispatch_group_async
			(	wG
			,	wQ
			,	^{	size_t	wS = i;
					size_t	wE = wNumUnits - 1;
					T		w = 0;
					while ( wS < wE )
					{	w += (T)wQU[ wS ] * (T)wQU[ wE ];
						wS++;
						wE--;
					}
					w <<= 1;
					if ( wS == wE ) w += (T)wQU[ wI / 2 ] * (T)wQU[ wI / 2 ];
					uTmp[ wI ] = w;
				}
			);
			wI++;
		}
		dispatch_group_wait( wG, DISPATCH_TIME_FOREVER );
#if !OS_OBJECT_USE_OBJC
		dispatch_release( wG );
#endif

		JpMooParaiso::Zero( wQU, wI + 3 );
		for ( size_t i = 0; i < wI; i++ ) *(T*)( wQU + i ) += uTmp[ i ];

		uNumBits *= 2;
		while (	! Bit( uNumBits - 1 ) ) uNumBits--;
	}

	void
	Square2()
	{
/*
11213 is Prime  Elappesed(4)
19937 is Prime  Elappesed(24)
21701 is Prime  Elappesed(31)
23209 is Prime  Elappesed(38)
44497 is Prime  Elappesed(272)
*/
		size_t	wNumUnits = ( uNumBits + ( NUM_HALF_BITS - 1 ) ) / NUM_HALF_BITS;
		JpMooParaiso::Zero( (HALF_T*)uTmp, wNumUnits * 2 );
		HALF_T*	wHU = (HALF_T*)u;
		HALF_T*	wHT = (HALF_T*)uTmp;

		size_t wI = 0;
		while ( wI < wNumUnits )
		{	size_t	wS = 0;
			size_t	wE = wI;
			while ( wS < wE )
			{	T	w = (T)wHU[ wS ] * (T)wHU[ wE ];
				T*	wDst = (T*)( wHT + wI );
				Add<T>( wDst, w );
				Add<T>( wDst, w );
				wS++;
				wE--;
			}
			if ( wS == wE ) Add<T>( (T*)( wHT + wI ), (T)wHU[ wI / 2 ] * (T)wHU[ wI / 2 ] );
			wI++;
		}
		for ( size_t i = 1; i < wNumUnits; i++ )
		{	size_t	wS = i;
			size_t	wE = wNumUnits - 1;
			while ( wS < wE )
			{	T	w = (T)wHU[ wS ] * (T)wHU[ wE ];
				T*	wDst = (T*)( wHT + wI );
				Add<T>( wDst, w );
				Add<T>( wDst, w );
				wS++;
				wE--;
			}
			if ( wS == wE ) Add<T>( (T*)( wHT + wI ), (T)wHU[ wI / 2 ] * (T)wHU[ wI / 2 ] );
			wI++;
		}

		T* wSwap = u;
		u = uTmp;
		uTmp = wSwap;
		uNumBits *= 2;
		while (	! Bit( uNumBits - 1 ) ) uNumBits--;
	}

	bool
	IsMersennePrime( uint64_t p )
	{	uNumBits = 3;
		u[ 0 ] = 4;
		for ( uint64_t i = 2; i < p; i++ )
		{
//printf( "    %llu      \r", i );
//fflush( stdout );

//#define DUMP
			SquareAsync();
#ifdef	DUMP
			printf( "%llu:", i );
			fflush( stdout );
			Dump();
#endif
			Sub<T>( u, 2 );
			while ( uNumBits && ! Bit( uNumBits - 1 ) ) uNumBits--;
#ifdef	DUMP
			printf( " - " );
			Dump();
#endif
			while ( uNumBits > p ) Fold( p );
#ifdef	DUMP
			printf( " - " );
			Dump();
			printf( "\n" );
#endif
		}
		for ( int i = 0; i < uNumBits / NUM_UNIT_BITS; i++ ) if ( u[ i ] != ALL_BITS ) return false;
		return uNumBits % NUM_UNIT_BITS
		?	u[ p / NUM_UNIT_BITS ] == ~ ( ALL_BITS << ( uNumBits % NUM_UNIT_BITS ) )
		:	true
		;
	}

};


//	1,000,000,000 * log2(10) = 3,321,928,094.887362
//	( 3,321,928,095 +  7 ) /  8 = 415,241,012
//	( 3,321,928,095 + 31 ) / 32 = 103,810,253
//	( 3,321,928,095 + 63 ) / 64 =  51,905,127

	#define	NUM_DIGITS	3321928095

//	100,000,000 * log2(10) = 332,192,809.4887362
//	( 332,192,810 +  7 ) /  8 = 41,524,102
//	( 332,192,810 + 31 ) / 32 = 10,381,026
//	( 332,192,810 + 63 ) / 64 =  5,190,513

//	#define	NUM_DIGITS	332192810



static	Bits< uint16_t, uint32_t, uint64_t, NUM_DIGITS * 8 >  sBits;



void
Test()
{
	uint64_t wPrimes[] =
	{	3
	,	5
	,	7
	,	13
	,	17
	,	19
	,	31
	,	61
	,	89
	,	107
	,	127
	,	521
	,	607
	,	1279
	,	2203
	,	2281
	,	3217
	,	4253
	,	4423
	,	9689
	,	9941
	,	11213
	,	19937
	,	21701
	,	23209
	,	44497
	,	86243
	,	110503
	,	132049
	,	216091
	,	756839
	,	859433
	,	1257787
	,	1398269
	,	2976221
	,	3021377
	,	6972593
	,	13466917
	,	20996100
	,	24036583
	,	25964951
	,	30402457
	,	32582657
	,	37156667
	,	42643801
	,	43112609
	,	57885161
	};
	for ( int i = 0; i < 48; i++ )
//	for ( int i = 30; i < 48; i++ )
	{	Timer wTimer;
		printf( "%d : %llu is %s  ", i, wPrimes[ i ], sBits.IsMersennePrime( wPrimes[ i ] ) ? "Prime" : "Composite" );
		printf( "Elappesed(%llu)\n", wTimer.Second() );
	}
}

int
main( int argc, const char * argv[] )
{
void
TestImm();
TestImm();

//	Test();

//	@autoreleasepool
//	{	Test();
//	}
//	return 0;
}
void
TestImm()
{
    unsigned int a[] = {3, 4, 0x7fffffff, 5, 6};
    unsigned int b[] = {1, 2, 0x7fffffff, 3, 4};
 	unsigned int z[ 4 ];
    int i;
	(__v4si&)z = (__v4si&)a + (__v4si&)b;

    for (i = 0; i < 4; i++) {
        printf("z[%i]: %i\n", i, z[3-i]);
    };  
}
/*
static	void
TestArithmetics()
{	Bits<uint8_t, uint16_t, uint32_t, 33> w;
	w.uNumBits = 32;
	w.Fill();
	w.Dump();
	printf( " " );
	w += 1;
	w.Dump();
	printf( " " );
	w -= 2;
	w.Dump();
	printf( "\n" );
}
static	void
Test16Sub( size_t pNumBits, size_t pBitIndex )
{	Bits<uint8_t, uint16_t, uint32_t, 16 * 4 + 1> w;
	w.uNumBits = pNumBits;
	w.Fill();
	w.Dump();
	printf( " %2zu ", pBitIndex );
	w.Fold( pBitIndex );
	w.Dump();
	printf( "\n" );
}

static	void
Test16()
{	Test16Sub(  2,  1 );
	Test16Sub( 14,  1 );
	Test16Sub( 14,  3 );
	Test16Sub( 14, 11 );
	Test16Sub( 14, 13 );
	Test16Sub( 15,  1 );
	Test16Sub( 15,  3 );
	Test16Sub( 15, 11 );
	Test16Sub( 15, 13 );
	Test16Sub( 16,  1 );
	Test16Sub( 16,  3 );
	Test16Sub( 16, 13 );
	Test16Sub( 16, 15 );
	Test16Sub( 17,  1 );
	Test16Sub( 17,  3 );
	Test16Sub( 17, 13 );
	Test16Sub( 17, 15 );
	Test16Sub( 30,  1 );
	Test16Sub( 30,  3 );
	Test16Sub( 30, 27 );
	Test16Sub( 30, 29 );
	Test16Sub( 31,  1 );
	Test16Sub( 31,  3 );
	Test16Sub( 31, 27 );
	Test16Sub( 31, 29 );
	Test16Sub( 32,  1 );
	Test16Sub( 32,  3 );
	Test16Sub( 32, 29 );
	Test16Sub( 32, 31 );
	Test16Sub( 33,  1 );
	Test16Sub( 33,  3 );
	Test16Sub( 33, 29 );
	Test16Sub( 33, 31 );
	Test16Sub( 47,  1 );
	Test16Sub( 47,  3 );
	Test16Sub( 47, 43 );
	Test16Sub( 47, 45 );
	Test16Sub( 48,  1 );
	Test16Sub( 48,  3 );
	Test16Sub( 48, 45 );
	Test16Sub( 48, 47 );
	Test16Sub( 49,  1 );
	Test16Sub( 49,  3 );
	Test16Sub( 49, 45 );
	Test16Sub( 49, 47 );
	Test16Sub( 50,  1 );
	Test16Sub( 50,  3 );
	Test16Sub( 50, 47 );
	Test16Sub( 50, 49 );
}
static	void
Test32Sub( size_t pNumBits, size_t pBitIndex )
{	Bits<uint16_t, uint32_t, uint64_t, 32 * 4 + 1> w( pNumBits );
	size_t	wElemIndex = pNumBits / 32;
	w.u[ wElemIndex ] = 0;
	for ( size_t i = wElemIndex * 32; i < pNumBits; i++ ) w.On( i );
	while ( wElemIndex-- ) w.u[ wElemIndex ] = 0xffffffff;
	w.Dump();
	printf( " %2zu ", pBitIndex );
	w.Fold( pBitIndex );
	w.Dump();
	printf( "\n" );
}

static	void
Test32()
{	Test32Sub(  2,  1 );
	printf( "\n" );

	Test32Sub( 31,  1 );
	Test32Sub( 31,  3 );
	Test32Sub( 31, 27 );
	Test32Sub( 31, 29 );
	printf( "\n" );

	Test32Sub( 32,  1 );
	Test32Sub( 32,  3 );
	Test32Sub( 32, 29 );
	Test32Sub( 32, 31 );
	printf( "\n" );

	Test32Sub( 33,  1 );
	Test32Sub( 33,  3 );
	Test32Sub( 33, 29 );
	Test32Sub( 33, 31 );
	printf( "\n" );

	Test32Sub( 63,  1 );
	Test32Sub( 63,  3 );
	Test32Sub( 63, 59 );
	Test32Sub( 63, 61 );
	printf( "\n" );

	Test32Sub( 64,  1 );
	Test32Sub( 64,  3 );
	Test32Sub( 64, 61 );
	Test32Sub( 64, 63 );
	printf( "\n" );

	Test32Sub( 65,  1 );
	Test32Sub( 65,  3 );
	Test32Sub( 65, 61 );
	Test32Sub( 65, 63 );
	printf( "\n" );

	Test32Sub( 95,  1 );
	Test32Sub( 95,  3 );
	Test32Sub( 95, 91 );
	Test32Sub( 95, 93 );
	printf( "\n" );

	Test32Sub( 96,  1 );
	Test32Sub( 96,  3 );
	Test32Sub( 96, 93 );
	Test32Sub( 96, 95 );
	printf( "\n" );

	Test32Sub( 97,  1 );
	Test32Sub( 97,  3 );
	Test32Sub( 97, 93 );
	Test32Sub( 97, 95 );
	printf( "\n" );
}


*/
