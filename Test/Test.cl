#define	unit1	ushort

kernel	void
Set( global unit1* p )
{
	size_t	i = get_global_id( 0 );

	if ( i % 2 ) p[ i ] = get_num_groups( 0 );
	switch ( i )
	{
	case	0:	p[ i ] = get_work_dim();		break;
	case	1:	p[ i ] = get_global_size( 0 );	break;
	case	2:	p[ i ] = get_num_groups( 0 );	break;
//	default:	p[ i ] = i;						break;
	}
}

kernel	void
Types( global ushort* p )
{	size_t	i = get_global_id( 0 );

	switch ( i % 8 )
	{
	case	0:	p[ i ] = 8 * sizeof( uchar );	break;
	case	1:	p[ i ] = 8 * sizeof( ushort );	break;
	case	2:	p[ i ] = 8 * sizeof( uint );	break;
	case	3:	p[ i ] = 8 * sizeof( ulong );	break;
	case	4:	p[ i ] = 8 * sizeof( ulong2 );	break;
	case	5:	p[ i ] = 8 * sizeof( ulong4 );	break;
	case	6:	p[ i ] = 8 * sizeof( ulong8 );	break;
	case	7:	p[ i ] = 8 * sizeof( ulong16 );	break;
	}
}

kernel	void
Div2( global ulong16* p )	//	1024 bit
{	p[ 1 ] = p[ 0 ] / 2;
}

kernel	void
Add( global ulong* pL, global ulong* pR, global ulong* pO )
{	size_t	i = get_global_id( 0 );
	pO[ i ] = pL[ i ] + pR[ i ];
}
