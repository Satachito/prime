
typedef	ushort	unit1_t;
typedef	uint	unit2_t;
typedef	ulong	unit4_t;

kernel	void
AddCarry
(	global	unit1_t*	pAugend
,			size_t		pAugendOffset
,	global	unit1_t*	pAddend
,			size_t		pAddendOffset
,	global	bool*		pHasCarry
)
{	size_t	i = get_global_id( 0 );
	pAugend[ i + pAugendOffset ] += pAddend[ i + pAddendOffset ];
	if ( pAugend[ i + pAugendOffset ] < pAddend[ i + pAddendOffset ] )
	{	pAddend[ i + pAddendOffset ] = 1;
		*pHasCarry = true;
	}
	else pAddend[ i + pAddendOffset ] = 0;
}

//						  5	  4	  3	  2	  1	  0
//						  5	  4	  3	  2	  1	  0
//---------------------------------------------
//	 55		 44		 33		 22		 11		 00
//		 54	 53	 52	 51	 50
//				 43	 42	 41	 40
//						 32	 31	 30
//								 21	 20
//										 10

kernel	void
Square1st
(	global	unit1_t*	p
,	global	unit4_t*	pW
,			size_t		pNumC
)
{
	size_t	i = get_global_id( 0 );
	size_t	wS;
	size_t	wE;
	if ( i < pNumC )
	{	wS = 0;
		wE = i;
	}
	else
	{	wS = i - pNumC + 1;
		wE = pNumC - 1;
	}
	unit4_t v = 0;
	while ( wS < wE ) v += ( (unit4_t)p[ wS++ ] * (unit4_t)p[ wE-- ] );
	v *= 2;
	if ( wS == wE ) v += ( (unit4_t)p[ wS ] * (unit4_t)p[ wE ] );
	pW[ i ] = v;
}


//  13  12  11  10   9   8   7   6   5   4   3   2   1   0
//--------------------------------------------------------
//                                   5   4   3   2   1   0
//                                   5   4   3   2   1   0
//--------------------------------------------------------
//              40  36  32  28  24  20  16  12   8   4   0
//          41  37  33  29  25  21  17  13   9   5   1
//          38  34  30  26  22  18  14  10   6   2
//          35  31  27  23  19  15  11   7   3
//--------------------------------------------------------

//                                                   1   0
//                                                   1   0
//--------------------------------------------------------
//                                               8   4   0
//                                           9   5   1
//                                           6   2
//                                           3
//--------------------------------------------------------

//                                               2   1   0
//                                               2   1   0
//--------------------------------------------------------
//                                      16  12   8   4   0
//                                  17  13   9   5   1
//                                  14  10   6   2
//                                  11   7   3


kernel	void
Square2nd
(	global	unit1_t*	pW
,	global	unit1_t*	p
,			size_t		pNumC
,			size_t		pCarryOffset
,	global	bool*		pHasCarry
)
{	switch ( get_global_size( 0 ) )
	{
	case 0:
		break;
	case 1:
		*(unit4_t*)p = *(unit4_t*)pW;
		break;
	default:
		{	unit2_t	w;
			size_t	i = get_global_id( 0 );
			switch ( i )
			{
			case 0:
				p[ 0 ] = pW[ 0 ];
				p[ pCarryOffset ] = 0;

				w
				=	(unit2_t)pW[ 1 ]
				+	(unit2_t)pW[ 4 ]
				;
				p[ 1 ] = *(unit1_t*)&w;
				if ( ( p[ 1 + pCarryOffset ] = *( (unit1_t*)&w + 1 ) ) ) *pHasCarry = true;

				p[ pNumC * 2 + 1 ] = pW[ pNumC * 8 - 5 ];
				p[ pNumC * 2 + 1 + pCarryOffset ] = 0;

				w
				=	(unit2_t)pW[ pNumC * 8 -  9 ]
				+	(unit2_t)pW[ pNumC * 8 -  6 ]
				;
				p[ pNumC * 2 ] = *(unit1_t*)&w;
				if ( ( p[ pNumC * 2 + pCarryOffset ] = *( (unit1_t*)&w + 1 ) ) ) *pHasCarry = true;

				break;
			case 1:
				w
				=	(unit2_t)pW[ 2 ]
				+	(unit2_t)pW[ 5 ]
				+	(unit2_t)pW[ 8 ]
				;
				p[ 2 ] = *(unit1_t*)&w;
				if ( ( p[ 2 + pCarryOffset ] = *( (unit1_t*)&w + 1 ) ) ) *pHasCarry = true;

				w
				=	(unit2_t)pW[ pNumC * 8 - 13 ]
				+	(unit2_t)pW[ pNumC * 8 - 10 ]
				+	(unit2_t)pW[ pNumC * 8 -  7 ]
				;
				p[ pNumC * 2 - 1 ] = *(unit1_t*)&w;
				if ( ( p[ pNumC * 2 - 1 + pCarryOffset ] = *( (unit1_t*)&w + 1 ) ) ) *pHasCarry = true;
				
				break;
			default:
				w
				=	(unit2_t)pW[ i * 8 - 13 ]
				+	(unit2_t)pW[ i * 8 - 10 ]
				+	(unit2_t)pW[ i * 8 -  7 ]
				+	(unit2_t)pW[ i * 8 -  4 ]
				;
				p[ i * 2 - 1 ] = *(unit1_t*)&w;
				if ( ( p[ i * 2 - 1 + pCarryOffset ] = *( (unit1_t*)&w + 1 ) ) ) *pHasCarry = true;

				w
				=	(unit2_t)pW[ i * 8 -  9 ]
				+	(unit2_t)pW[ i * 8 -  6 ]
				+	(unit2_t)pW[ i * 8 -  3 ]
				+	(unit2_t)pW[ i * 8 -  0 ]
				;
				p[ i * 2 ] = *(unit1_t*)&w;
				if ( ( p[ i * 2 + pCarryOffset ] = *( (unit1_t*)&w + 1 ) ) ) *pHasCarry = true;
				break;
			}
		}
		break;
	}
}

//	The global size must be ceiled.
kernel	void
Fold1st
(	global	unit1_t*	p0
,	global	unit1_t*	p1
,			size_t		pFromUnitF
,			size_t		pNumShifts
)
{	size_t	i = get_global_id( 0 );
	p1[ i ]
	=	( p0[ i + pFromUnitF ] >> pNumShifts )
	|	( p0[ i + pFromUnitF + 1 ] << sizeof( unit1_t ) * 8 - pNumShifts )
	;
}

//	The global size must be ceiled.
kernel	void
Fold2nd
(	global	unit1_t*	p0
,			size_t		pFromUnitF
,			size_t		pNumShifts
)
{	size_t	i = get_global_id( 0 );
	if ( i )
	{	p0[ pFromUnitF + i ] = 0;
	}
	else
	{	p0[ pFromUnitF ] &= ((unit1_t)-1) >> sizeof( unit1_t ) * 8 - pNumShifts;
		p0[ pFromUnitF + get_global_size( 0 ) ] &= ((unit1_t)-1) << pNumShifts;
	}
}

//	The global size must be ceiled.
kernel	void
LLTestBits
(	global	unit1_t*	p
,			size_t		pNumShifts
,	global	bool*		pFail
)
{	size_t	i = get_global_id( 0 );
	if ( i )
	{	if ( p[ i - 1 ] != (unit1_t)-1 ) pFail[ 0 ] = true;
	}
	else
	{	if ( p[ get_global_size( 0 ) - 1 ] != ( ((unit1_t)-1) >> ( sizeof( unit1_t ) * 8 - pNumShifts ) ) ) pFail[ 0 ] = true;
	}
}



