#import		"AppDelegate.h"

#include			<JpMooParaiso/Generic.h>
using	namespace	JpMooParaiso;

#include	<OpenCL/opencl.h>
#include	"OpenCL.h"

typedef	cl_ushort		unit1_t;
typedef	cl_uint			unit2_t;
typedef	cl_ulong		unit4_t;

inline	size_t
NumUnit1Bits()
{	return sizeof( unit1_t ) * 8;
}
inline	size_t
NumUnit2Bits()
{	return sizeof( unit2_t ) * 8;
}
inline	size_t
NumUnit4Bits()
{	return sizeof( unit4_t ) * 8;
}
//	1,000,000,000 * log2(10) = 3,321,928,094.887362
//	( 3,321,928,095 +  7 ) /  8 = 415,241,012
//	( 3,321,928,095 + 31 ) / 32 = 103,810,253
//	( 3,321,928,095 + 63 ) / 64 =  51,905,127

//	#define	NUM_BITS			size_t( 3321928095LL )

//	100,000,000 * log2(10) = 332,192,809.4887362
//	( 332,192,810 +  7 ) /  8 = 41,524,102
//	( 332,192,810 + 31 ) / 32 = 10,381,026
//	( 332,192,810 + 63 ) / 64 =  5,190,513

	#define	NUM_BITS			size_t( 332192810LL )
	#define	NUM_BUFFER_BYTES	NUM_BITS
	//	NUM_BITS / 8 * 8 ( for squaring )

//	FED^2 = FDA169
//	FFEEDD^2 = FFDDBB25AAC9
//	https://developer.apple.com/library/mac/documentation/Performance/Conceptual/OpenCL_MacProgGuide/ExampleHelloWorld/Example_HelloWorld.html#//apple_ref/doc/uid/TP40008312-CH112-SW2

#include	"Kernel.cl.h"

template	<typename T>	T
N( T p )
{	assert( p );
	return p;
}

struct
Buffer
{	void*	m;
	size_t	mSize;
	virtual	~
	Buffer()
	{	gcl_free( m );
	}
	Buffer( size_t p )
	:	m( N( gcl_malloc( p, NULL, 0 ) ) )
	,	mSize( p )
	{
	}
	operator	bool*()		{ return (bool*)m; }
	operator	unit1_t*()	{ return (unit1_t*)m; }
	operator	unit2_t*()	{ return (unit2_t*)m; }
	operator	unit4_t*()	{ return (unit4_t*)m; }
	template	<typename T>	T
	Get( size_t pOffset )
	{	T	v;
		size_t	wDstOrigin[] = { 0, 0, 0 };
		size_t	wSrcOrigin[] = { 0, 0, pOffset };
		size_t	wRegion[] = { sizeof( T ), 1, 1 };
		
		gcl_memcpy_rect
		(	&v
		,	m
		,	wDstOrigin
		,	wSrcOrigin
		,	wRegion
		,	0
		,	0
		,	0
		,	0
		);
		return v;
	}
	template	<typename T>	void
	Put( size_t pOffset, T const& p )
	{	size_t	wDstOrigin[] = { 0, 0, pOffset };
		size_t	wSrcOrigin[] = { 0, 0, 0 };
		size_t	wRegion[] = { sizeof( T ), 1, 1 };
		
		gcl_memcpy_rect
		(	m
		,	&p
		,	wDstOrigin
		,	wSrcOrigin
		,	wRegion
		,	0
		,	0
		,	0
		,	0
		);
	}
};
/*
struct
Mapped
{	void*	m;
	~
	Mapped()
	{	gcl_unmap( m );
	}
	Mapped( void* pBuffer, size_t pSize, cl_map_flags pFlags = CL_MAP_READ )
	:	m( N( gcl_map_ptr( pBuffer, pFlags, pSize ) ) )
	{
	}
	Mapped( Buffer const& p, cl_map_flags pFlags = CL_MAP_READ )
	:	Mapped( p.m, p.mSize, pFlags )
	{
	}
	operator	bool*()		{ return (bool*)m; }
	operator	uint64_t*()	{ return (uint64_t*)m; }
	operator	uint16_t*()	{ return (uint16_t*)m; }
};
*/
struct
LLTester
{	dispatch_queue_t	mQ;
	Buffer				m;
	Buffer				mW;
	Buffer				mFlag;

#ifdef	DEBUG
	Buffer				mExtra;
#endif

	size_t				mNumBits;

	void
	Dump()
	{	printf( ":%zu\n", mNumBits );

		size_t wIndex = 16;
		unit1_t w[ wIndex ];
		gcl_memcpy( w, m.m, sizeof( w ) );
		while( wIndex-- ) printf( "%08x ", w[ wIndex ] );
		printf( "\n\n" );
	}

	void
	DumpW()
	{	printf( "------------------------------\n" );

		size_t wIndex = 16;
		unit1_t w[ wIndex ];
		gcl_memcpy( w, mW.m, sizeof( w ) );
#ifdef	CPU
		while( wIndex-- ) printf( "%08x ", w[ wIndex ] );
#endif
#ifdef	GPU
		while( wIndex-- ) printf( "%08llx %08llx %08llx %08llx ", w[ wIndex ].s[ 0 ], w[ wIndex ].s[ 1 ], w[ wIndex ].s[ 2 ], w[ wIndex ].s[ 3 ] );
#endif
		printf( "\n\n" );
	}
	LLTester( int p = CL_DEVICE_TYPE_GPU, cl_device_id pID = NULL )
	:	mQ( N( gcl_create_dispatch_queue( p, pID ) ) )
	,	m( NUM_BUFFER_BYTES )
	,	mW( NUM_BUFFER_BYTES )
	,	mFlag( 1 * sizeof( bool ) )
#ifdef	DEBUG
	,	mExtra( 4 * sizeof( uint16_t ) )
#endif
 	{	printf( "NUM_BUFFER_BYTES:%ld\n", NUM_BUFFER_BYTES );
//		assert( sizeof( size_t ) == 8 );
//		assert( sizeof( long ) == 8 );
//		assert( sizeof( bool ) == 1 );
	}
	void
	AdjustNumBits()
	{	while ( mNumBits )
		{	uint64_t w = m.Get<uint64_t>( ( mNumBits - 1 ) / 64 );
			uint64_t wMask = uint64_t( 1 ) << ( ( mNumBits - 1 ) % 64 );
			if ( w & wMask ) break;
			mNumBits--;
		}
	}
	
	inline	size_t
	NumUnit1sC()
	{	return ( mNumBits + NumUnit1Bits() - 1 ) / NumUnit1Bits();
	}
	
	inline	size_t
	NumUnit2sC()
	{	return ( mNumBits + NumUnit2Bits() - 1 ) / NumUnit2Bits();
	}
	
	inline	size_t
	NumUnit4sC()
	{	return ( mNumBits + NumUnit4Bits() - 1 ) / NumUnit4Bits();
	}
	
	void
	Test( size_t p )
	{	dispatch_sync
		(	mQ
		,	^{	Timer wTimer;
				unit1_t w4 = 4;
				gcl_memcpy( m.m, &w4, sizeof( w4 ) );
				mNumBits = 3;

				cl_ndrange wNDRange = { 1, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 } };
bool	wTracing = false;
				for ( size_t i = 2; i < p; i++ )
				{
//wTracing
//=	m.Get<unit1_t>( 0 ) == 0x17637
//||	m.Get<unit1_t>( 0 ) == 0x9d38
//;
					{	size_t	wNumUnit1s = NumUnit1sC();
						{	wNDRange.global_work_size[ 0 ] = wNumUnit1s * 2 - 1;
							Square1st_kernel
							(	&wNDRange
							,	(unit1_t*)m
							,	(unit4_t*)mW
							,	wNumUnit1s
							);
						}
						{	wNDRange.global_work_size[ 0 ] = wNumUnit1s;
							mFlag.Put( 0, false );
							Square2nd_kernel
							(	&wNDRange
							,	(unit1_t*)mW
							,	(unit1_t*)m
							,	wNumUnit1s
							,	wNumUnit1s * 4
							,	mFlag
							);
						}
//printf( "%x %x %x %x\n", (int)mExtra.Get<uint16_t>( 0 ), (int)mExtra.Get<uint16_t>( 1 ), (int)mExtra.Get<uint16_t>( 2 ), (int)mExtra.Get<uint16_t>( 3 ) );
						if ( mFlag.Get<bool>( 0 ) )
						{	size_t wOffset = 0;
							wNDRange.global_work_size[ 0 ] = wNumUnit1s * 2 + 2 - 1;
							do
							{	mFlag.Put( 0, false );
								AddCarry_kernel
								(	&wNDRange
								,	(unit1_t*)m
								,	++wOffset
								,	(unit1_t*)m
								,	wNumUnit1s * 4
								,	mFlag
								);
							} while ( mFlag.Get<bool>( 0 ) );
						}
					}
if ( wTracing )
{	printf( "After Square" );
	Dump();
	DumpW();
}
//	Sub2
					uint64_t w = m.Get<uint64_t>( 0 );
					m.Put<uint64_t>( 0, w - 2 );
					switch ( w )
					{
					case 0:
					case 1:
						{	size_t	wIndex = 1;
							do
							{	w = m.Get<uint64_t>( wIndex );
								m.Put( wIndex, w - 1 );
								wIndex++;
							} while ( ! w );
						}
						break;
					default:
						break;
					}
					mNumBits *= 2;
					AdjustNumBits();
if ( wTracing )
{	printf( "After Minus 2" );
	Dump();
}

//	Fold
					while ( mNumBits > p )
					{
assert( mNumBits - p <= p );
						size_t	wNumUnt1sC = NumUnit1sC();
						m.Put( wNumUnt1sC, unit1_t( 0 ) );
						size_t	wFromUnit1 = p / NumUnit1Bits();
						size_t	wNumShiftBits = p % NumUnit1Bits();
						wNDRange.global_work_size[ 0 ] = wNumUnt1sC;
						Fold1st_kernel( &wNDRange, (unit1_t*)m, (unit1_t*)mW, wFromUnit1, wNumShiftBits );
						Fold2nd_kernel( &wNDRange, (unit1_t*)m, wFromUnit1, wNumShiftBits );
						size_t wOffset = 0;
						do
						{	mFlag.Put( 0, false );
							AddCarry_kernel
							(	&wNDRange
							,	(unit1_t*)m
							,	wOffset++
							,	(unit1_t*)mW
							,	0
							,	mFlag
							);
						} while ( mFlag.Get<bool>( 0 ) );
						mNumBits = p + 1;
						AdjustNumBits();
if ( wTracing )
{	printf( "After Adding" );
	Dump();
}
					}
//Dump();
				}
//	Test
				bool	wLLOK;
				if ( mNumBits == p )
				{	mFlag.Put( 0, false );
					wNDRange.global_work_size[ 0 ] = NumUnit1sC();
					LLTestBits_kernel( &wNDRange, (unit1_t*)m, p % NumUnit1Bits(), mFlag );
					wLLOK = ! mFlag.Get<bool>( 0 );
				}
				else
				{	wLLOK = false;
				}
				NSLog( @"Elappesed(%llu)\n", wTimer.Milli() );
				NSLog
				(	@"2^%zu - 1 is %@\n"
				,	p
				,	wLLOK ? @"a prime --------------------" : @"not a prime"
				);
			}
		);
	}
};
/*
MacBook Air 11inch 2013 late using CPU
NumPlatforms：1
	Platform:Apple
	NumDevices:2
		ID:ffffffff	Device:Intel(R) Core(TM) i5-4250U CPU @ 1.30GHz
		Max units:4
		Max dimensions:3
		Max group size:1024
			Work item size:1024
			Work item size:1
			Work item size:1

		ID:1024500	Device:HD Graphics 5000
		Max units:280
		Max dimensions:3
		Max group size:512
			Work item size:512
			Work item size:512
			Work item size:512

NUM_BUFFER_BYTES:332192810
Elappesed(16)
2^3 - 1 is a prime --------------------
Elappesed(0)
2^5 - 1 is a prime --------------------
Elappesed(0)
2^7 - 1 is a prime --------------------
Elappesed(1)
2^13 - 1 is a prime --------------------
Elappesed(1)
2^17 - 1 is a prime --------------------
Elappesed(2)
2^19 - 1 is a prime --------------------
Elappesed(3)
2^31 - 1 is a prime --------------------
Elappesed(7)
2^61 - 1 is a prime --------------------
Elappesed(12)
2^89 - 1 is a prime --------------------
Elappesed(14)
2^107 - 1 is a prime --------------------
Elappesed(22)
2^127 - 1 is a prime --------------------
Elappesed(123)
2^521 - 1 is a prime --------------------
Elappesed(143)
2^607 - 1 is a prime --------------------
Elappesed(292)
2^1279 - 1 is a prime --------------------
Elappesed(521)
2^2203 - 1 is a prime --------------------
Elappesed(533)
2^2281 - 1 is a prime --------------------
Elappesed(944)
2^3217 - 1 is a prime --------------------
Elappesed(1108)
2^4253 - 1 is a prime --------------------
Elappesed(1141)
2^4423 - 1 is a prime --------------------
Elappesed(3542)
2^9689 - 1 is a prime --------------------
Elappesed(3670)
2^9941 - 1 is a prime --------------------
Elappesed(4500)
2^11213 - 1 is a prime --------------------
Elappesed(13644)
2^19937 - 1 is a prime --------------------
Elappesed(17871)
2^21701 - 1 is a prime --------------------
Elappesed(18931)
2^23209 - 1 is a prime --------------------
Elappesed(94536)
2^44497 - 1 is a prime --------------------
*/


int lucas(int p)
{
    char *l, *m;
    char *lp, *mp1, *mp2;
    int  i, j, k;
    int  ca, x;
    int  ret = 1;

    if (p <= 0) return 0;
    if (p <= 2) return 1;

    if ((l = (char *)malloc(p)) == NULL) return -1;
    if ((m = (char *)malloc(p)) == NULL) {
        free(l);
        return -1;
    }

    for (lp = l + p; lp != l; ) *--lp = 0;
    *(l+2) = 1;                                     /* L(1) = 4 */

	uint64_t wCount = 0;
    for (i = 2; i < p; i++) {
        for (lp = l + p, mp1 = m + p; lp != l; ) {
            *--mp1 = *--lp;
            *lp = 1;                        /* 2^p -1 mod 2^p -1 = 0 */
        }
        *(l+1) = 0;                                 /* L(i) = L(i) - 2 */
        for (mp1 = m, j = 0; j < p; j++) {
            if (*mp1++) {                           /* this bit is 1 */
                ca = 0;
                k = j;
                for (mp2 = m; mp2 != m + p; ) {     /* L(i) * L(i) */
                    x = *mp2++ + *(l+k) + ca;
                    *(l+k) = x & 1;
                    ca = x >> 1;
                    if (++k == p) k = 0;
                }
                if (ca) {
                    while (*(l+k)) {
                        *(l+k) = 0;
                        if (++k == p) k = 0;
                    }
                    *(l+k) = 1;
                }
            }
        }
{	uint64_t w = 0;
	for ( uint64_t i = 0; i < p; i++ )
	{	if ( ( i % 64 ) == ( p % 64 ) )
		{	printf( "%16llx ", w );
			w = 0;
		}
		if ( l[ p - 1 - i ] )
		{	w |= ( 1LL << ( ( p - 1 - i ) % 64 ) );
		}
	}
	if ( p % 64 ) printf( "%16llx ", w );
	printf( "    ------ %llu\n", ++wCount + 2 );
}
	}

    x = *l;
    for (lp = l + p; lp != l; )               /* L(p-1) == all 0 or 1 ? */
        if (*--lp != x) {
            ret = 0;
            break;
        }

    free(m);
    free(l);
    return ret;
}


@implementation
AppDelegate


-	(void)
applicationDidFinishLaunching:(NSNotification*)p
{	Log( @"Start" );
	uint64_t sMs[] =
	{	3
	,	5
	,	7
	,	13
	,	17
	,	19
	,	31
	,	61
	,	89
	,	107
	,	127
	,	521
	,	607
	,	1279
	,	2203
	,	2281
	,	3217
	,	4253
	,	4423
	,	9689
	,	9941
	,	11213
	,	19937
	,	21701
	,	23209
	,	44497
	,	86243
	,	110503
	,	132049
	,	216091
	,	756839
	,	859433
	,	1257787
	,	1398269
	,	2976221
	,	3021377
	,	6972593
	,	13466917
	,	20996100
	,	24036583
	,	25964951
	,	30402457
	,	32582657
	,	37156667
	,	42643801
	,	43112609
	,	57885161
	};
//	printf( "lucas( 44497 ):%s\n", lucas( 44497 ) ? "Prime" : "Composite" );
//	printf( "Inspecting %llu is prime or not.\n", wPrimes[ i ] );
	Dump();
	LLTester wLLTester;
//	for ( int i = 0; i < 48; i++ )
	for ( int i = 0; i < 48; i++ )
	{
//		lucas( (int)sMs[ i ] );
		wLLTester.Test( sMs[ i ] );
//		printf( "%llu is %s ", sMs[ i ], IsMersennePrime( wPrimes[ i ] ) ? "Prime" : "Composite" );
	}
}

@end
